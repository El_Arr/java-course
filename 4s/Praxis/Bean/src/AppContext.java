import hackage.BeanDefinition;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import java.lang.reflect.Method;
import java.util.HashMap;
import java.util.Map;
import java.util.Set;

public class AppContext {
    private Map<String, Object> beanMap;

    public AppContext(String packageName) {
        Map<String, BeanDefinition> beanDefinitionMap = createBeanDefinitions(packageName);
        createBeans(beanDefinitionMap);
    }

    private void createBeans(Map<String, BeanDefinition> beanDefinitionMap) {
        beanMap = new HashMap<>();
        for (String beanName : beanDefinitionMap.keySet()) {
            if (beanMap.get(beanName) == null) {
                Class cl = beanDefinitionMap.get(beanName.getClassType());
                Object o = null;
                try {
                    o = cl.newInstance();
                    for (Method method: cl.getMethods()) {
                        if (method.getName().startsWith("set") && method.isAnnotationPresent(Autowired.class)) {
                            /*
                            ToDo
                            - найти зависимость в параметре setter
                            - найти созданный Beam
                            - если нет, то создаём
                            - внедряем Bean, вызывая сеттер с помощью invoke
                             */
                        }
                    }
                } catch (IllegalAccessException | InstantiationException e) {
                    e.printStackTrace();
                }
                beanMap.put(beanName, o);
            }
        }
    }

    public Map<String, BeanDefinition> createBeanDefinitions(String packageName) {
        Map<String, BeanDefinition> beanDefs = new HashMap<>();
        Reflections reflections = new Reflections(packageName);
        Set<Class<?>> annotated = reflections.getTypesAnnotatedWith(Component.class);

    }

    public <T> T getBean(Class<T> tClass) {
        String beanName = tClass.getName();
        if (beanMap.containsKey(beanName)) {
            return (T) beanMap.get(beanName);
        }

    }

}
