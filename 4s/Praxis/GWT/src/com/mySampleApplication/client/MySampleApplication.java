package com.mySampleApplication.client;

import com.google.gwt.core.client.EntryPoint;
import com.google.gwt.user.client.ui.*;
import com.google.gwt.user.client.rpc.AsyncCallback;
import com.google.gwt.user.client.DOM;
import com.google.gwt.event.dom.client.ClickHandler;
import com.google.gwt.event.dom.client.ClickEvent;
import com.mySampleApplication.server.MySampleApplicationServiceImpl;

/**
 * Entry point classes define <code>onModuleLoad()</code>
 */
public class MySampleApplication implements EntryPoint {

    /**
     * This is the entry point method.
     */
    public void onModuleLoad() {
        TextBox first = new TextBox();
        first.setTitle("Enter the first part here");

        ListBox action = new ListBox();
        action.addItem("+");
        action.addItem("-");
        action.addItem("*");
        action.addItem("/");

        TextBox second = new TextBox();
        second.setTitle("Enter the second part here");

        Button button = new Button("Calculate");
        Label label = new Label();

        button.addClickHandler(event -> {
            String s = action.getSelectedItemText();
            double fv = Double.parseDouble(first.getText());
            double sv = Double.parseDouble(second.getText());
            MySampleApplicationServiceImpl.calculate(
                    Double.parseDouble(first.getText(), action.getSelectedItemText(), Double.parseDouble(second.getText()))
            );
        });

        RootPanel.get("first").add(button);
        RootPanel.get("action").add(action);
        RootPanel.get("second").add(second);
        RootPanel.get("button").add(button);
        RootPanel.get("result").add(label);
    }

    private static class MyAsyncCallback implements AsyncCallback<String> {
        private Label label;

        public MyAsyncCallback(Label label) {
            this.label = label;
        }

        public void onSuccess(String result) {
            label.getElement().setInnerHTML(result);
        }

        public void onFailure(Throwable throwable) {
            label.setText("Failed to receive answer from server!");
        }
    }
}
