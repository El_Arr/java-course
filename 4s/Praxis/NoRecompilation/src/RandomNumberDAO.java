import java.security.SecureRandom;

public class RandomNumberDAO implements NumberDAO {

    public int getAnyNumber() {
        return (new SecureRandom()).nextInt();
    }

}
