package ru.kpfu.itis.el_arr.exam.task5.server;

import ru.kpfu.itis.el_arr.exam.task5.client.Client;
import ru.kpfu.itis.el_arr.exam.task5.util.Constants;
import ru.kpfu.itis.el_arr.exam.task5.util.ObjectExchanger;

import java.io.IOException;
import java.net.ServerSocket;
import java.util.ArrayList;
import java.util.List;

import static ru.kpfu.itis.el_arr.exam.task5.util.Constants.MIN_PLAYER_NUM;
import static ru.kpfu.itis.el_arr.exam.task5.util.Constants.PORT;

public class Server {
    private static List<Client> clients;
    private static boolean full = false;
    private static boolean playing = false;

    public static void main(String[] args) {
        clients = new ArrayList<>();
        start();
    }

    public static List<Client> getClients() {
        return clients;
    }

    private static void notifyAllClients(Object o) {
        for (Client client : clients) {
            ObjectExchanger.send(client.getSocket(), o);
        }
    }

    private static void notifyClient(Client client, Object o) {
        ObjectExchanger.send(client.getSocket(), o);
    }

    public static void start() {
        try {
            ServerSocket serverSocket = new ServerSocket(PORT);
            System.out.println(Constants.Interactions.WAITING_SERVER);
            Client client;
            while (clients.size() <= MIN_PLAYER_NUM) {
                client = new Client(serverSocket.accept());
                notifyClient(client, Constants.Interactions.WAITING_PLAYER);
                clients.add(client);
            }
            notifyAllClients(Constants.Interactions.PLAYER_GREETING);
            full = true;
            playing = true;
            game();
        } catch (IOException e) {
            System.out.println(Constants.Interactions.ERROR_SERVER);
        }
    }

    private static void game() {
        String s;
        while (playing) {
            notifyAllClients(playing);
            s = (String) ObjectExchanger.get(clients.get(0).getSocket());
            notifyClient(clients.get(1), s);
            s = (String) ObjectExchanger.get(clients.get(1).getSocket());
            notifyClient(clients.get(0), s);
        }
    }

    public static boolean isFull() {
        return full;
    }

    public static void setFull(boolean full) {
        Server.full = full;
    }

    public static void setPlaying(boolean playing) {
        Server.playing = playing;
    }
}
