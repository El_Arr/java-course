package ru.kpfu.itis.el_arr.exam.task5.util;

import java.io.IOException;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.net.Socket;

public class ObjectExchanger {

    public static Object get(Socket socket) {
        try {
            ObjectInputStream objectInputStream = new ObjectInputStream(
                    socket.getInputStream()
            );
            return objectInputStream.readObject();
        } catch (IOException | ClassNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }

    public static void send(Socket socket, Object o) {
        try {
            ObjectOutputStream objectOutputStream = new ObjectOutputStream(
                    socket.getOutputStream()
            );
            objectOutputStream.writeObject(o);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}
