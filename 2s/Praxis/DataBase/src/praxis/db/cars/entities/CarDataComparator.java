package praxis.db.cars.entities;

import java.util.Comparator;

public class CarDataComparator implements Comparator<CarData> {
    private String field;

    public CarDataComparator(String field) throws NoSuchFieldException {
        Class<CarDataComparator> c = CarDataComparator.class;
        System.out.println(this.field = c.getDeclaredField(field).getName());
    }

    @Override
    public int compare(CarData o1, CarData o2) {
        switch (field) {
            case "id":
                return (new Integer(o1.getId())).compareTo(o2.getId());
            case "mpg":
                return (new Double(o1.getMpg())).compareTo(o2.getMpg());
            case "cylinders":
                return (new Integer(o1.getCylinders())).compareTo(o2.getCylinders());
            case "horsepower":
                return (new Integer(o1.getHorsepower())).compareTo(o2.getHorsepower());
            case "weight":
                return (new Integer(o1.getWeight())).compareTo(o2.getWeight());
            case "accelerate":
                return (new Double(o1.getAccelerate())).compareTo(o2.getAccelerate());
            case "year":
                return (new Integer(o1.getYear())).compareTo(o2.getYear());
        }
        return 0;
    }

    public boolean equals(Object obj) {
        return false;
    }
}
