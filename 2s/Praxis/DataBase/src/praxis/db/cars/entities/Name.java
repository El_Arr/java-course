package praxis.db.cars.entities;

public class Name {
    private int id;
    private String modelName;
    private Model model;
    private String make;

    public Name() {
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public Model getModel() {
        return model;
    }

    public void setModel(Model model) {
        this.model = model;
    }

    public String getMake() {
        return make;
    }

    public void setMake(String make) {
        this.make = make;
    }

    @Override
    public String toString() {
        return "Name{" +
                "id=" + id +
                ", modelName='" + modelName + '\'' +
                ", model=" + model.getModel() +
                ", make='" + make + '\'' +
                '}';
    }
}
