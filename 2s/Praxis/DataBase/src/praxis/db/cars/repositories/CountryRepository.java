package praxis.db.cars.repositories;

import praxis.db.cars.DataBaseConnection;
import praxis.db.cars.entities.CarMaker;
import praxis.db.cars.entities.Country;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;

public class CountryRepository {
    private DataBaseConnection dbConnection;

    public CountryRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public List<Country> getAll() {
        return dbConnection.getDBConnection().getAllCountries();
    }

    public Country getByName(String name) {
        for (Country country : dbConnection.getDBConnection().getAllCountries()) {
            if (country.getCountryName().equals(name)) {
                return country;
            }
        }
        return null;
    }

    public List<Country> getByContinentNames(List<String> listOfContinents) {
        List<Country> result = new ArrayList<>();
        for (Country country : DataBaseConnection.getDBConnection().getAllCountries()) {
            if (listOfContinents.contains(country.getContinent().getContinent())) {
                result.add(country);
            }
        }
        return result;
    }
    
}
