package praxis.db.cars.repositories;

import praxis.db.cars.DataBaseConnection;
import praxis.db.cars.entities.Model;

import java.util.List;

public class ModelRepository {
    private DataBaseConnection dbConnection;

    public ModelRepository(DataBaseConnection dbConnection) {
        this.dbConnection = dbConnection;
    }

    public List<Model> getAll() {
        return dbConnection.getDBConnection().getAllModels();
    }
}
