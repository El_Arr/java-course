package praxis.db.cars;

import praxis.db.cars.entities.*;

import java.io.*;
import java.util.LinkedList;
import java.util.List;
import java.util.Scanner;

public class DataBase {
    private List<Country> countries;
    private List<Continent> continents;
    private List<CarMaker> carMakers;
    private List<Model> models;
    private List<Name> names;
    private List<CarData> cars;

    void init() {
        prepContinents("lib/continents.csv");
        prepCountries("lib/countries.csv");
        prepCarMakers("lib/car-makers.csv");
        prepModels("lib/model-list.csv");
        prepNames("lib/car-names.csv");
        prepCars("lib/cars-data.csv");
    }

    private void prepCarMakers(String filename) {
        carMakers = new LinkedList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(new File(filename)))) {
            try {
                String args[];
                String s = in.readLine();
                while ((s = in.readLine()) != null) {
                    args = s.split(",");
                    carMakers.add(
                            new CarMaker(
                                    Integer.parseInt(args[0]),
                                    args[1].substring(1, args[1].length() - 1),
                                    args[2].substring(1, args[2].length() - 1),
                                    Integer.parseInt(args[3]),
                                    getCountryById(Integer.parseInt(args[3]))
                            )
                    );
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage() + "404, collection is empty");
        }
    }

    private void prepContinents(String filename) {
        continents = new LinkedList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(new File(filename)))) {
            try {
                String args[];
                String s = in.readLine();
                while ((s = in.readLine()) != null) {
                    args = s.split(",");
                    continents.add(new Continent(Integer.valueOf(args[0]), args[1].substring(1, args[1].length() - 1)));
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage() + "404, collection is empty");
        }
    }

    private void prepCountries(String filename) {
        countries = new LinkedList<>();
        try (BufferedReader in = new BufferedReader(new FileReader(new File(filename)))) {
            try {
                String args[];
                String s = in.readLine();
                while ((s = in.readLine()) != null) {
                    args = s.split(",");
                    Country country = new Country(
                            Integer.valueOf(args[0]),
                            args[1].substring(1, args[1].length() - 1),
                            Integer.valueOf(args[2])
                    );
                    country.setContinent(getContinentById(country.getContinentId()));
                    countries.add(country);
                }
            } finally {
                in.close();
            }
        } catch (IOException e) {
            System.out.println(e.getMessage() + "404, collection is empty");
        }
    }

    private void prepModels(String filename) {
        models = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] strData = scanner.nextLine().split(",");
                Model model = new Model();
                model.setModelId(Integer.parseInt(strData[0]));
                model.setCarMakerId(Integer.parseInt(strData[1]));
                model.setCarMaker(getCarMakerById(model.getCarMakerId()));
                model.setModel(strData[2].substring(1, strData[2].length() - 1));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, file not found, collection is empty");
        }
    }

    private void prepNames(String filename) {
        names = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] strData = scanner.nextLine().split(",");
                Name name = new Name();
                name.setId(Integer.parseInt(strData[0]));
                name.setModelName(strData[1].substring(1, strData[1].length() - 1));
                name.setModel(getModelByModelName(name.getModelName()));
                name.setMake(strData[2].substring(1, strData[2].length() - 1));
            }
        } catch (FileNotFoundException e) {
            System.out.println("Sorry, file not found, collection is empty");
        }
    }

    private void prepCars(String filename) {
        cars = new LinkedList<>();
        try {
            Scanner scanner = new Scanner(new File(filename));
            scanner.nextLine();
            while (scanner.hasNext()) {
                String[] stData = scanner.nextLine().split(",");
                cars.add(new CarData(
                        Integer.parseInt(stData[0]),
                        getMakerByCarId(Integer.parseInt(stData[0])),
                        Double.parseDouble(stData[1]),
                        Integer.parseInt(stData[2]),
                        Double.parseDouble(stData[3]),
                        Integer.parseInt(stData[4]),
                        Integer.parseInt(stData[5]),
                        Double.parseDouble(stData[6]),
                        Integer.parseInt(stData[7])
                ));
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public Continent getContinentById(int id) {
        for (Continent continent : continents) {
            if (continent.getContId() == id)
                return continent;
        }
        return null;
    }

    public Country getCountryById(int id) {
        for (Country country : countries) {
            if (country.getCountryId() == id) return country;
        }
        return null;
    }

    public CarMaker getCarMakerById(int id) {
        for (CarMaker carMaker : carMakers) {
            if (carMaker.getId() == id) return carMaker;
        }
        return null;
    }

    public Model getModelByModelName(String modelName) {
        for (Model model : models) {
            if (model.getModel().equals(modelName)) return model;
        }
        return null;
    }

    public Name getMakerByCarId(int id) {
        for (Name name : names) {
            if (name.getId() == id) return name;
        }
        return null;
    }

    public List<Continent> getAllContinents() {
        return continents;
    }

    public List<Country> getAllCountries() {
        return countries;
    }

    public List<CarMaker> getAllCarMakers() {
        return carMakers;
    }

    public List<Model> getAllModels() {
        return models;
    }

    public List<Name> getAllNames() {
        return names;
    }

    public List<CarData> getAllCars() {
        return cars;
    }
}
