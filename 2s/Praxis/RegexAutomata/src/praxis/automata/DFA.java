package praxis.automata;

import java.util.*;

public class DFA {
    private String alphabet;
    private Map<InputAndStateDFA, List<State>> transitions = new TreeMap<>();
    private States finalState;
    private States startState;

    public DFA(NFA nfa) {
        alphabet = nfa.getAlphabet();
        startState = new States();
        startState.add(nfa.getStartState());
        Queue<States> q = new LinkedList<>();
        q.offer(startState);
        Map<InputAndStateNFA, List<State>> nfaTr = nfa.getTransitions();
        while (!q.isEmpty()) {
            States from = q.poll();
            States to = new States();
            for (int i = 0; i < alphabet.length(); i++) {
                Character c = alphabet.charAt(i);
                for (State state : from) {
                    InputAndStateNFA ias = new InputAndStateNFA(c, state);
                    List<State> lst = nfaTr.get(ias);
                    for (State s : lst) {
                        to.add(s);
                    }
                }
                InputAndStateDFA ias = new InputAndStateDFA(c, from);
            }
        }
    }


}
