package praxis.automata;

public class HardcodedAutomata {

    boolean checkByAutomata(int[][] transitions, int[] finalStates, String input) {
        boolean found = false;
        int t, k = 0;
        for (int i = 0; i < input.length(); i++) {
            t = input.charAt(i);
            for (int j = 0; j < transitions[k].length && !found; j++) {
                if (transitions[k][j] == t) {
                    found = true;
                    k = j;
                }
            }
            if (!found) {
                return false;
            }
        }
        for (int i = 0; i < finalStates.length; i++) {
            if (finalStates[i] == k) {
                return true;
            }
        }
        return false;
    }

}