package praxis.adder;

public class Adder extends Thread {
    private static int collector = 0;

    public Adder(int[] a, int lowerBound, int upperBound) {
        for (int i = lowerBound; i < upperBound; i++) {
            collector += a[i];
        }
    }

    public static int getCollector() {
        return collector;
    }
}
