package praxis.game.characters.enemy.creators;

import praxis.game.characters.EnemyCharacter;
import praxis.game.characters.enemy.Reaper;

public class ReaperCreator extends EnemyCharCreator {

    @Override
    public EnemyCharacter getNew() {
        return new Reaper();
    }
}
