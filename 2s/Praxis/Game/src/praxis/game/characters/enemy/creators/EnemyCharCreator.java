package praxis.game.characters.enemy.creators;

import praxis.game.characters.EnemyCharacter;

public abstract class EnemyCharCreator {

    public abstract EnemyCharacter getNew();

}
