package praxis.game.characters;

import praxis.game.armory.AttackArmory;

public class PlayerCharacter extends Character {
    private AttackArmory currentArmory;

    @Override
    public void attack(Character c) {
        currentArmory.attack(c);
    }

    public void setCurrentArmory(AttackArmory attackArmory) {

    }

}