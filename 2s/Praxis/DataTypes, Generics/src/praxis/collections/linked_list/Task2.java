package praxis.collections.linked_list;

import java.util.Scanner;

public class Task2 {
    public static void main(String[] args) {
        Elem head1 = null;
        Elem head2 = null;
        Elem p = new Elem();
        Scanner in = new Scanner(System.in);

        System.out.println("Введите размер первого списка");
        int n = in.nextInt();
        System.out.println("Введите " + n + " элементов");
        for (int i = 0; i < n; i++) {
            p = new Elem(in.nextInt(), head1);
            head1 = p;
        }

        System.out.println("Введите размер второго списка");
        n = in.nextInt();
        System.out.println("Введите " + n + " элементов");
        for (int i = 0; i < n; i++) {
            p = new Elem(in.nextInt(), head2);
            head2 = p;
        }

        Elem pr = new Elem();
        p = head1;
        while (p != null) {
            pr = p;
            p = p.getNext();
        }
        pr.setNext(head2);
        System.out.println("Все элементы нового списка");
        p = head1;
        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }
    }
}
