package praxis.collections.linked_list;

import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
        Elem head = null;
        Elem p;

        int n = 5;
        Scanner in = new Scanner(System.in);

        for (int i = 0; i < n; i++) {
            p = new Elem<Integer>(in.nextInt(), head);
            head = p;
        }

        p = head;
        while (p != null) {
            System.out.println(p.getValue());
            p = p.getNext();
        }

        int s = 0;
        for (p = head; p != null; p = p.getNext()) {
            s += Integer.valueOf(p.getValue().toString());
        }

        System.out.println(s);
    }
}
