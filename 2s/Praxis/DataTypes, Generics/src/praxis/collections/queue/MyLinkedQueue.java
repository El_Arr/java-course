package praxis.collections.queue;

public class MyLinkedQueue<T> implements MyQueue<T> {
    private Elem<T> head;
    private Elem<T> tail;

    public void push(T o) {
        Elem<T> n = new Elem<>(o, null);
        tail.setNext(n);
        tail = n;
    }

    public T poll() {
        Elem<T> res = head;
        head = head.getNext();
        return res.getValue();
    }

    public boolean isEmpty() {
        return head == null;
    }
}