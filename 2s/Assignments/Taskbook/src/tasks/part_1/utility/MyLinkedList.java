package tasks.part_1.utility;

import javafx.util.Pair;

import java.util.Comparator;
import java.util.Scanner;

/**
 * @author Eldar Mingachev
 *         KPFU ITIS 11-601
 */

public class MyLinkedList<T> {
    private Elem<T> head;
    private Elem<T> last;

    //task001
    public MyLinkedList(Scanner in) {
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            this.add((T) (in.next()));
        }
        System.out.println(this);
    }

    //task002
    public T maxValue() {
        Elem<T> temp = head;
        T max = temp.getValue();
        Comparator<T> c = Comparator.comparing(t -> Integer.valueOf(t.toString()));
        while (temp != null) {
            if (c.compare(max, temp.getValue()) < 0) {
                max = temp.getValue();
            }
            temp = temp.getNext();
        }
        return max;
    }

    //task003
    public Pair<Integer, Integer> sumAndMult() {
        Elem<Integer> temp = (Elem<Integer>) head;
        int sum = 0;
        int mult = 1;
        while (temp.getNext() != null) {
            sum += temp.getValue();
            mult *= temp.getValue();
            temp = temp.getNext();
        }
        return new Pair<>(sum, mult);
    }

    //task005
    public MyLinkedList concatTask(MyLinkedList list) {
        this.concat(list);
        return this;
    }

    //task006
    public void swapValues() {
        T headValue = head.getValue();
        head.setValue(last.getValue());
        last.setValue(headValue);
    }

    //task009
    public void reverse() {
        Elem<T> last = this.last, head = this.head, temp, temp1;
        last.setNext(head);
        temp = head.getNext();
        head.setNext(null);
        head = temp;
        while (head != this.last) {
            temp = last.getNext();
            last.setNext(head);
            temp1 = head;
            head = head.getNext();
            temp1.setNext(temp);
        }
        temp = this.head;
        this.head = this.last;
        this.last = temp;
    }

    //task012
    public void shiftToEnd(int k) {
        last.setNext(head);
        for (int c = 0; c <= k; c++) {
            move1pos(head, last);
        }
        last.setNext(null);
    }

    private void move1pos(Elem<T> tempH, Elem<T> tempL) {
        tempH = tempH.getNext();
        tempL = tempL.getNext();
        head = tempH;
        last = tempL;
    }

    //task013
    public void shiftToHead(int k) {
        this.reverse();
        last.setNext(head);
        for (int c = 0; c <= k; c++) {
            move1pos(head, last);
        }
        last.setNext(null);
        this.reverse();
    }

    public MyLinkedList() {
    }

    public MyLinkedList(Elem head) {
        this.head = head;
        Elem p = head;
        while (p != null) {
            this.last = p;
            p = p.getNext();
        }
    }

    public MyLinkedList(T x) {
        this.add(x);
    }

    public void add(T x) {
        if (head == null) {
            head = new Elem<>(x, null);
            last = head;
        } else {
            Elem p = new Elem<>(x, null);
            last.setNext(p);
            last = p;
        }
    }

    public void addHead(T x) {
        head = new Elem<>(x, head);
    }

    public void insert(int i, T x) {
        Elem<T> temp = head;
        if (i < length()) {
            if (i == 0) {
                head = new Elem<>(x, head);
            } else {
                Elem<T> prev = temp;
                for (int c = 0; c < i && temp != null; c++) {
                    prev = temp;
                    temp = temp.getNext();
                }
                prev.setNext(new Elem<>(x, temp));
            }
        } else {
            temp = new Elem<>(x, null);
            last.setNext(temp);
            last = temp;
        }
    }

    public boolean removeByIndex(int i) {
        if (i == 0) {
            removeHead();
            return true;
        } else if (i < length()) {
            Elem prev = head;
            Elem temp = head;
            for (int c = 0; c < i; c++) {
                prev = temp;
                temp = temp.getNext();
            }
            prev.setNext(temp.getNext());
            return true;
        }
        return false;
    }

    public boolean removeByValue(T x) {
        if (head.getValue().equals(x)) {
            removeHead();
            return true;
        }
        Elem prev = head;
        Elem temp = head.getNext();
        while (temp != null) {
            if (temp.getValue().equals(x)) {
                temp = temp.getNext();
                prev.setNext(temp);
                return true;
            } else {
                prev = temp;
                temp = temp.getNext();
            }
        }
        return false;
    }


    public boolean removeAllByValue(T x) {
        boolean flag = false;
        if (head.getValue().equals(x)) {
            removeHead();
            flag = true;
        }
        Elem prev = head;
        Elem temp = head.getNext();
        while (temp != null) {
            if (temp.getValue().equals(x)) {
                temp = temp.getNext();
                prev.setNext(temp);
                flag = true;
            } else {
                prev = temp;
                temp = temp.getNext();
            }
        }
        return flag;
    }

    public boolean removeByLinks(Elem<T> prev, Elem<T> temp) {
        if (prev == temp) {
            removeHead();
        } else {
            prev.setNext(temp.getNext());
        }
        return true;
    }

    public boolean removeAfter(T x) {
        Elem temp = head;
        while (temp != null) {
            if (temp.getValue().equals(x)) {
                temp.setNext(temp.getNext().getNext());
                return true;
            } else {
                temp = temp.getNext();
            }
        }
        return false;
    }

    private void removeHead() {
        if (head.getNext() == null) {
            last = null;
            head = null;
        } else {
            head = head.getNext();
        }
    }

    public void concat(MyLinkedList l) {
        this.last.setNext(l.head);
        Elem p = l.head;
        while (p != null) {
            this.last = p;
            p = p.getNext();
        }
    }

    public int length() {
        int count = 0;
        Elem<T> temp = head;
        while (temp != null) {
            temp = temp.getNext();
            count++;
        }
        return count;
    }

    public Elem<T> getHead() {
        return head;
    }

    public void setHead(Elem<T> head) {
        this.head = head;
    }

    public Elem<T> getLast() {
        return last;
    }

    public void setLast(Elem<T> last) {
        this.last = last;
    }

    @Override
    public String toString() {
        String res = "[ ";
        Elem<T> e = this.getHead();
        while (e.getNext() != null) {
            res += e.toString() + ", ";
            e = e.getNext();
        }
        res += e.toString() + " ";
        return res + "]";
    }
}
