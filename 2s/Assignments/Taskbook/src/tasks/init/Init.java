package tasks.init;

import tasks.part_1.utility.MyLinkedList;

import java.util.LinkedList;
import java.util.Scanner;

/**
 * @author Eldar Mingachev
 *         KPFU ITIS 11-601
 */
public class Init {

    public static LinkedList<Integer> linkedListInit(Scanner in) {
        LinkedList<Integer> list = new LinkedList<>();
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            list.add(in.nextInt());
        }
        return list;
    }

    public static MyLinkedList<Integer> myLinkedListInit(Scanner in) {
        MyLinkedList<Integer> list = new MyLinkedList<>();
        int n = in.nextInt();
        for (int i = 0; i < n; i++) {
            list.add(in.nextInt());
        }
        return list;
    }

}
