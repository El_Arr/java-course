package ru.kpfu.itis.el_arr.stackoverflowsort;

import java.util.ArrayList;
import java.util.Scanner;

public class Controller {

    public static void main(String[] args) {
        ReadingAndWriting.generate();
        String type = userInput();
        String mainURL = "http://stackoverflow.com/questions/tagged/sorting+java+integer?sort=votes";

        ArrayList<String> links = Searcher.retrieveLinks(mainURL);
        boolean gotcha = false;
        int i = 1;
        for (int k = 0; k < 2 && !gotcha; k++) {
            if (k == 1 && (type.equals("LinkedList") || type.equals("ArrayList"))) {
                type = "Array";
                System.out.println("Для введённой структуры данных сортировок не найдено. Поиск по \"" + type + "\"...");
            }
            for (i = 0; i < links.size() && !gotcha; i++) {
                String snippet = Searcher.retrieveCodeSnippet(links.get(i), type);
                if (snippet != null) {
                    ReadingAndWriting.codeToFile(snippet, type);
                    compileAndRun();
                    if (sorted()) {
                        gotcha = true;
                    }
                }
            }
        }
        if (gotcha) {
            System.out.println(type + " из *res/array.txt* отсортирован.\n" +
                    "Код взят из комментария по ссылке:\n" + links.get(i - 1) + "\n" +
                    "Результат можно посмотреть в *res/result.txt*");
        } else
            System.out.println("Данные не были отсортированы. Ни один из фрагментов кода в комментариях не подошёл");
    }

    private static int runProcess(String command) throws Exception {
        java.lang.Process pro = Runtime.getRuntime().exec(command);
        pro.waitFor();
        return pro.exitValue();
    }

    private static void compileAndRun() {
        try {
            int k = runProcess("javac -sourcepath src -d out src/ru/kpfu/itis/el_arr/stackoverflowsort/Run.java");
            if (k == 0) {
                k = runProcess("java -classpath out ru.kpfu.itis.el_arr.stackoverflowsort.Run");
            }
            if (k != 0) {
                System.err.println("Ошибка! Подходящего кода не найдено или неверен формат запроса");
                System.exit(0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private static String userInput() {
        System.out.println("Какую структуру данных нужно отсортировать? [Array/ ArrayList/ LinkedList]");
        return (new Scanner(System.in)).nextLine();
    }

    private static boolean sorted() {
        int[] a = ReadingAndWriting.readToArray("result");
        for (int i = 0; i < a.length - 1; i++) {
            if (a[i] > a[i + 1]) {
                return false;
            }
        }
        return true;
    }
}