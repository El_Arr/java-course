package ru.kpfu.itis.el_arr.stackoverflowsort;

import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;

import java.io.IOException;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

class Searcher {

    private static ArrayList<String> retrieveHREFs(String url) {
        ArrayList<String> res = new ArrayList<>();
        try {
            Document doc = Jsoup.connect(url).get();
            Elements links = doc.select("a[href]");
            Pattern p = Pattern.compile("<a href=\"/questions/\\d*/.*\" class=\"question-hyperlink\">.*</a>");
            Matcher m;
            for (Element elem : links) {
                m = p.matcher(elem.toString());
                if (m.matches()) {
                    res.add(elem.toString());
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return res;
    }

    private static ArrayList<String> retrieveURLs(ArrayList<String> arrayList) {
        ArrayList<String> res = new ArrayList<>(arrayList.size());
        Pattern p = Pattern.compile("/\\d+/(\\w+-?)+");
        for (String s : arrayList) {
            Matcher m = p.matcher(s);
            if (m.find()) {
                res.add("http://stackoverflow.com/questions" + m.group());
            }
        }
        return res;
    }

    static String retrieveCodeSnippet(String url, String type) {
        try {
            final int CUT = 6;
            Document doc = Jsoup.connect(url).get();
            Elements snippets = doc.select("div[id=\"answers\"]").select("code");
            Pattern p;
            if (type.equals("ArrayList") || type.equals("LinkedList")) {
                p = Pattern.compile(".* " + type + "<Integer> .*\\(.* " + type + "<Integer> .*\\)");
            } else {
                p = Pattern.compile(".* int\\[] .*\\(.* int\\[] .*\\)");
            }
            for (Element elem : snippets) {
                String[] arr = elem.toString().split("\n");
                for (int i = 0; i < 7 && i < arr.length; i++) {
                    Matcher m = p.matcher((arr[i]));
                    if (m.find()) {
                        String s = elem.toString().substring(CUT, elem.toString().length() - CUT - 1);
                        s = s.replaceAll("(&lt;)", "<").replaceAll("(&gt;)", ">");
                        return s;
                    }
                }
            }
        } catch (IOException e) {
            System.out.println(e.getMessage());
        }
        return null;
    }

    static ArrayList<String> retrieveLinks(String mainURL) {
        return retrieveURLs(Searcher.retrieveHREFs(mainURL));
    }
}
