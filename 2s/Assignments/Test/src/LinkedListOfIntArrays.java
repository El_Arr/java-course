import java.util.Arrays;
import java.util.Scanner;

public class LinkedListOfIntArrays {
    private Elem head;
    private Elem last;

    public LinkedListOfIntArrays() {
    }

    public LinkedListOfIntArrays(Elem head) {
        this.head = head;
        Elem p = head;
        while (p != null) {
            this.last = p;
            p = p.getNext();
        }
    }

    public LinkedListOfIntArrays(Integer[] x) {
        head = new Elem(x, null);
        last = head;
    }

    public void add(Integer[] x) {
        if (this.head == null) {
            addHead(x);
        } else {
            Elem p = new Elem(x, null);
            last.setNext(p);
            last = p;
        }
    }

    public void addHead(Integer[] x) {
        Elem p = new Elem(x, head);
        head = p;
    }

    public void show() {
        Elem temp = this.head;
        while (temp != null) {
            System.out.println(Arrays.toString(temp.getValue()));
            System.out.println();
            temp = temp.getNext();
        }
    }

    public static void main(String[] args) {
        Scanner in = new Scanner(System.in);
        System.out.println("Введите количество массивов в списке");
        int gl = in.nextInt();
        int n;
        Integer[] a;
        LinkedListOfIntArrays list = new LinkedListOfIntArrays();
        for (int i = 0; i < gl; i++) {
            System.out.println("Введите количество элементов в " + (i+1) + "-ом массиве");
            n = in.nextInt();
            a = new Integer[n];
            for (int j = 0; j < n; j++) {
                a[j] = in.nextInt();
            }
            list.add(a);
        }
        findTwoNear(list);
        copyPositive(list);
        list.show();
    }

    public static void findTwoNear(LinkedListOfIntArrays list) {
        Elem temp = list.head;
        double max = temp.average();
        temp = temp.getNext();
        double t = temp.average();
        double t1;
        max += t;
        temp = temp.getNext();
        int counter = 2; //counter и counter+1
        int saved;
        while (temp != null) {
            t1 = temp.average();
            if (t1 + t > max) {
                max = t1 + t;
                saved = counter;
            }
            t = t1;
            temp = temp.getNext();
            counter++;
        }
        System.out.println(counter + " и " + (counter + 1));
    }

    public static void copyPositive(LinkedListOfIntArrays list) {
        Elem temp = list.head;
        Elem next = temp.getNext();
        Integer[] t;
        Elem k;
        while (temp != null) {
            if (temp.allPositive()) {
                t = temp.copy();
                k = new Elem(t, null);
                temp.setNext(k);
                k.setNext(next);
            }
            temp = next;
            next = temp.getNext();
        }
    }
}