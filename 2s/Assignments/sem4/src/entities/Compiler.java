package entities;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Pattern;

public class Compiler {

    private static int mark_brill(String s) {
        int res = 1;
        int mult = 1;
        int pre = Integer.parseInt(s);
        for (int i = 0; i < s.length(); i++) {
            int elem = pre % 10;
            pre /= 10;
            elem *= mult;
            mult *= 2;
            res += elem;
        }
        return res;
    }

    private static int mark_exclaim(String s) {
        int num = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                num++;
            }
        }
        return num;
    }

    private static int mark_percent(String s) {
        int num = 0;
        for (int i = 0; i < s.length(); i++) {
            if (s.charAt(i) == '1') {
                num++;
            } else if (s.charAt(i) == '0') {
                num--;
            }
        }
        return num;
    }

    public static String[] compile(String input) {
        String[] splitted = input.split(";");
        Map<String, String> operators = new HashMap<>();

        for (int i = 0; i < splitted.length; i++) {
            String[] parts = splitted[i].split(":=");
            if (Pattern.compile("[A-Z]{2}").matcher(parts[0]).matches()) {
                if (operators.containsKey(parts[0])) {
                    operators.replace(parts[0], operators.get(parts[0]), parts[1]);
                } else {
                    operators.put(parts[0], parts[1]);
                }
            } else {
                if (parts[1].substring(1).matches("[A-Z]{2}")) {
                    if (operators.containsKey(parts[1].substring(1))) {
                        operators.put(parts[0],
                                operation("" + parts[1].charAt(0) + operators.get(parts[1].substring(1)), i));
                    } else {
                        System.err.println("Недопустимое присваивание! Переменная "
                                + parts[1].substring(1)
                                + " в строке " + i + " не была объявлена ранее");
                    }
                } else {
                    operators.put(parts[0], operation(parts[1], i));
                }
            }
        }
        String[] out = new String[splitted.length];
        int i = 0;
        for (Map.Entry<String, String> entry : operators.entrySet()) {
            out[i] = entry.getKey() + ":=" + entry.getValue() + "\n";
            i++;
        }
        return out;
    }

    private static String operation(String string, int i) {
        String mark = string.charAt(0) + "";
        if (mark.equals("◊")) {
            return "" + mark_brill(string.substring(1));
        } else if (mark.equals("!")) {
            return "" + mark_exclaim(string.substring(1));
        } else if (mark.equals("%")){
            return "" + mark_percent(string.substring(1));
        } else {
            System.err.println("Отсутствует знак в объявлении идентификатора. Строка " + i);
            System.exit(1);
            return null;
        }
    }
}