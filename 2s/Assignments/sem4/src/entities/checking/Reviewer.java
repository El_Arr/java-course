package entities.checking;

public class Reviewer {

    public static boolean review(String string) {
        string = string.replaceAll("\n","");
        String[] forSLCheck = string.split(";");
        String s = forSLCheck[0];
        int i = 1;
        while (SyntaxLexisChecker.checkOperator(s).getKey() && i < forSLCheck.length) {
            s = forSLCheck[i];
            i++;
        }
        String str = SyntaxLexisChecker.checkOperator(s).getValue();
        if (!str.equals("")) {
            System.err.println(str + " в строке " + (i - 1));
            System.exit(1);
        }

        s = forSLCheck[0];
        i = 1;
        while (SemanticChecker.checkOperator(s).getKey() && i < forSLCheck.length) {
            s = forSLCheck[i];
            i++;
        }
        str = SemanticChecker.checkOperator(s).getValue();
        if (!str.equals("")) {
            System.err.println(str + " в строке " + (i - 1));
            System.exit(1);
        }
        return true;
    }

}