package entities;

import java.io.*;

public class FileManager {
    private static File output = new File("res/output.txt");
    private static File input = new File("res/input.txt");

    public static String read() {
        if (output.exists()) {
            output.delete();
        }
        String res = "";
        String s;
        try (BufferedReader br = new BufferedReader(new FileReader(input))) {
            while ((s = br.readLine()) != null) {
                res += s;
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
        return res;
    }

    public static void write(String[] s) {
        try (BufferedWriter bw = new BufferedWriter((new FileWriter(output)))) {
            for (int i = 0; i < s.length; i++) {
                s[i] = s[i].trim();
                bw.write(s[i] + ';' + '\n');
            }
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

}