import javax.swing.*;
import java.awt.*;
import java.awt.event.MouseEvent;
import java.awt.event.MouseListener;

public class FirstWindow extends JFrame {
    private JPanel panel = new JPanel();
    private JPanel upperPanel = new JPanel();
    private JPanel lowerPanel = new JPanel();

    public FirstWindow() {
        createGUI();
    }

    public void createGUI() {
        setDefaultCloseOperation(WindowConstants.EXIT_ON_CLOSE);
        setResizable(true);

        panel.setBorder(BorderFactory.createEmptyBorder(5, 3, 5, 3));
        panel.setLayout(new BorderLayout());

        setContentPane(panel);

        JTextField field = new JTextField(35);
        field.setBounds(25, 25, 200, 100);
        field.setEditable(true);

        JButton reset = new JButton("C");
        reset.addMouseListener(new MouseListener() {
            public void mouseClicked(MouseEvent e) {
                JButton button = (JButton) e.getSource();
                if (button.equals(reset)) {
                    field.setText("");
                }
            }

            public void mouseEntered(MouseEvent e) {
            }

            public void mouseExited(MouseEvent e) {
            }

            public void mousePressed(MouseEvent e) {
            }

            public void mouseReleased(MouseEvent e) {
            }
        });
        reset.setBounds(250, 25, 100, 50);

        upperPanel.setLayout(new FlowLayout());
        upperPanel.add(field);
        upperPanel.add(reset);

        DefaultListModel<JButton> model = new DefaultListModel<>();
        model.add(0, new JButton("ABC"));
        model.add(1, new JButton("GHI"));
        model.add(2, new JButton("DEF"));
        model.add(3, new JButton("MNO"));
        model.add(4, new JButton("PQRS"));
        model.add(5, new JButton("JKL"));
        model.add(6, new JButton("TUV"));
        model.add(7, new JButton("WXYS"));

        lowerPanel.setLayout(new FlowLayout());
        for (int i = 0; i < model.getSize(); i++) {
            lowerPanel.add(model.getElementAt(i));
            model.getElementAt(i).addMouseListener(new MouseListener() {
                @Override
                public void mouseClicked(MouseEvent e) {
                    boolean found = false;
                    JButton button = (JButton) e.getSource();
                    JButton butt = new JButton();
                    for (int i = 1; i < model.getSize() && !found; i++) {
                        butt = model.getElementAt(i);
                        if (button.equals(butt)) {
                            found = true;
                        }
                    }
                    int n = e.getClickCount();
                    field.setText(field.getText() + butt.getText().charAt(n - 1));
                }

                public void mousePressed(MouseEvent e) {

                }

                public void mouseReleased(MouseEvent e) {

                }

                public void mouseEntered(MouseEvent e) {

                }

                public void mouseExited(MouseEvent e) {

                }
            });
        }


        panel.add(upperPanel, BorderLayout.NORTH);
        panel.add(lowerPanel, BorderLayout.SOUTH);

        setPreferredSize(new Dimension(500, 150));
        pack();
    }
}
