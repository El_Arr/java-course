public class Vector2D{

	private double x,y;
	
	public Vector2D(double x, double y){
		this.x = x;
		this.y = y;
	}
	public Vector2D add(Vector2D v){
		Vector2D result = new Vector2D(x + v.getX(), y + v.getY());
		return result;
	}
	public double getX() {
		return x;
	}
	public double getY() {
		return y;
	}
	public double Vector2D scalarProduct(Vector2D v1,Vector2D v2) {
		return v1.getX*v2.getX+v1.getY*v2.getY;
	}
	public String toString(){
		return "<" + x + ", " + y + ">";
	}

}