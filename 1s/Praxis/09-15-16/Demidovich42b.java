public class Demidovich42b{
	public static void main(String [] args) {
	
		double eps = Double.parseDouble(args[0]);
		double xp = 0;
		double xn = 1;
		double n = 1;
		
		do {
			n++;
			xp = xn;
			xn = (2*n) / (n*n*n + 1);
		}
		while (Math.abs(xn - xp) > eps);
		System.out.println(Math.abs(xn - xp));
	
	}
}