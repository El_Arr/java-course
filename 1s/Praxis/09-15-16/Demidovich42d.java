public class Demidovich42d{
	public static void main(String [] args) {
	
		double eps = Double.parseDouble(args[0]);
		double s = 1;
		double xp = 0;
		double xn = 0;
		double n = 1;
		double p = 1;
		
		do {
			n++;
			xp = xn;
			s = s * (-1);
			p = p * 0.999;
			xn = s * p;
		}
		while (Math.abs(xn - xp) > eps);
		System.out.println(Math.abs(xn - xp));
	
	}
}