public class Demidovich55{

	public static void main(String [] args) {
	
		double eps = Double.parseDouble(args[0]);
		
		double xp = 0;
		
		double xn = 0;
		
		double ch = 0;
		
		double zn = 1;
		
		double n = 0;
		
		do {
			
			n++;
			
			ch += (2 * n - 1);
			
			zn *= 2;
			
			xp = xn;
			
			xn = ch / zn;
			
		}
		
		while (Math.abs(xn - xp) > eps);
		
		System.out.println(Math.abs(xn - xp));
	
	}

}