public class Demidovich51{

	public static void main(String [] args) {
	
		double eps = Double.parseDouble(args[0]);
		
		double xp = 0;
		
		double xn = 0;
		
		double sum = 0;
		
		double n = 0;
		
		do {
			
			n++;
			
			sum = sum + n;
			
			xp = xn;
			
			xn = sum / (n * n);
			
		}
		
		while (Math.abs(xn - xp) > eps);
		
		System.out.println(Math.abs(xn - xp));
	
	}

}