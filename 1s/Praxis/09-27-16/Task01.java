import java.util.Scanner;

public class Task01{
	public static void main(String [] args) {
	
		Scanner in = new Scanner(System.in);
		int i = 0;
		int c;
		boolean fl = true;
		
		while (fl) {
			c = in.nextInt();
			i++;
			if (c > 0){
				fl = false;
			}
			if (!fl){
				System.out.println("+");
			}
			else{
				System.out.println("-");
			}
		}

	}
}