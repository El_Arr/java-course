import java.util.Scanner;

public class Sin{
	public static void main(String [] args) {
		Scanner in = new Scanner(System.in);
		System.out.println("Введите аргумент(в радианах)");
		double x =  in.nextDouble();
		final double eps = 1E-9;
		double i=0;
		double ch=1;
		double zn=1;
		double cur = -1;
		double pr = -1;
		do {
			if ((pr == -1)&&(cur == -1)){
				i = 2;
				ch = x*x;
				zn = 2;
				cur = x;
			}
			pr = cur;
			i++;
			zn*= i;
			ch*= x;
			if (i%2==1) {
				ch*= -1;
				cur+= ch/zn;
			}
		}
		while (Math.abs(pr - cur) > eps);
		System.out.println(cur);
	}
}