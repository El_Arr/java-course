public class Task00{

	public static void main(String [] args) {
	
		int n = Integer.parseInt(args[0]);
		
		double x = Double.parseDouble(args[1]);
		
		double sum = 0;
		
		double fact = 1;
		
		int i;
		
		for (i = 1; i <= n; i++) {
			
			fact *= i;
			
			sum += (x + 2) / fact;
		
		}
		
		System.out.println(sum);
		
	}
	
}