public class TriForce{

	public static void main(String [] args) {
		
		double n = Double.parseDouble(args[0]);
		
		double countSymbU = 1;
		
		double countSymbD = 1;
		
		double countIn = n;
		
		double countOut = 2 * n + 1;
		
		int i, j;
		
		for (i = 0; i <= 2 * n; i++) {
			
			for (j = 0; j < countOut; j++) {
				
				System.out.print(" ");
				
			}
			
			countOut--;
			
			if (i <= n) {
				
				for (j = 0; j < countSymbU; j++) {
				
					System.out.print("*");
				
				}
			
				countSymbU += 2;
			
			}
			
			if (i > n) {
				
				for (j = 0; j < countSymbD; j++) {
				
					System.out.print("*");
				
				}
				
				for (j = 0; j < countIn * 2 + 1; j++) {
				
					System.out.print(" ");
				
				}
			
				countIn -= 1;
				
				for (j = 0; j < countSymbD; j++) {
				
					System.out.print("*");
				
				}
				
				countSymbD = countSymbD + 2;
			
			}
		
			System.out.println("");
		
		}
		
		for (i = 0; i <= 4 * n + 2; i++) {
			
			System.out.print("*");
			
		}
	
	}
	
}