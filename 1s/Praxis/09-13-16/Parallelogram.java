public class Parallelogram{

	public static void main(String [] args) {
	
		double x = Double.parseDouble(args[0]);
		
		double y = Double.parseDouble(args[1]);
		
		double c = x;
		
		for (double i = y; i >= 0; i--){
			
			c--;
			
			double c0 = c;
			
			for (double j = x + c - 1; j >= 0; j--){
			
				if (c0 > 0) {
					
					System.out.print(" ");
					
					c0--;
				}
					
				else if (c0 == 0) {
					
					System.out.print("x");
					
				}
			
			}
			
			System.out.println();
			
		}
	
	}
}