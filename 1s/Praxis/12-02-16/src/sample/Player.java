package sample;

import javafx.scene.Group;

public class Player {
    // private int hp, scores;
    private double x, y, v;
    private VisualPlayer vp;

    public Player(int x, int y, Group group) {
        this.x = x;
        this.y = y;
        this.v = 10;
        // this.hp = 100;
        // this.scores = 0;
        vp = new VisualPlayer(group, this);
    }

    public void updateX(int dir) {
        this.x = this.x + dir * v;
        vp.updateX();
    }

    public void updateY(int dir) {
        this.y = this.y + dir * v;
        vp.updateY();
    }

    public double getX() {
        return x;
    }

    public double getY() {
        return y;
    }

}
