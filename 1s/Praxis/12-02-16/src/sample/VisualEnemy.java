package sample;

import javafx.scene.Group;
import javafx.scene.shape.Circle;

public class VisualEnemy {
    private final int RADIUS = 30;
    private Circle circle;
    private Enemy enemy;

    public VisualEnemy(Group root, Enemy e) {
        this.enemy = e;
        circle = new Circle(e.getX(), e.getY(), RADIUS);
        circle.setStyle("-fx-fill: darkviolet");
        root.getChildren().add(circle);
    }

    public void updateX() {
        circle.setCenterX(enemy.getX());
    }

    public void updateY() {
        circle.setCenterY(enemy.getY());
    }

    public int getRADIUS() {
        return RADIUS;
    }

    public Circle getCircle() {
        return circle;
    }
}
