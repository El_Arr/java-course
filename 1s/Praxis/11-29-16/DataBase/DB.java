import java.io.IOException;
import java.util.Scanner;

public class DB {
    public static void main(String[] args) throws IOException {
        final short RMSCAPACITY = 100;
        final int RSVCAPACITY = 1000;
        Room[] rooms = new Room[RMSCAPACITY];
        Reservation[] reservations = new Reservation[RSVCAPACITY];
        Scanner sc = new Scanner("Rooms.csv");
        int rsvLength = 0, rmsLength = 0;
        int i = 0;
        String s;

        try {
            while (sc.hasNextLine()) {
                s = sc.nextLine();
                if (i == 0) {
                    continue;
                } else {
                    rooms[i] = new Room(s);
                    rmsLength++;
                }
                i++;
            }
            sc = new Scanner("Reservations.csv");
            i = 0;
            while (sc.hasNextLine()) {
                s = sc.nextLine();
                if (i == 0) {
                    continue;
                } else {
                    reservations[i] = new Reservation(s, rooms);
                    rsvLength++;
                }
                i++;
            }
            sc.close();
        } catch (Exception ex) {
            System.out.println(ex.getMessage());
        }
        priceSort(rooms, 0, rmsLength);
        for (i = 0; i < rmsLength; i++) {
            System.out.println(rooms[i].getRoomName());
        }
    }

    private static void priceSort(Room[] arr, int start, int end) {
        if (start >= end)
            return;
        int i = start, j = end;
        int cur = i - (i - j) / 2;
        while (i < j) {
            while ((i < cur) && (arr[i].priceEquals(arr[cur]) || arr[i].priceLower(arr[cur]))) {
                i++;
            }
            while ((j > cur) && (arr[cur].priceEquals(arr[j]) || arr[cur].priceLower(arr[j]))) {
                j--;
            }
            if (i < j) {
                Room temp = arr[i];
                arr[i] = arr[j];
                arr[j] = temp;
                if (i == cur)
                    cur = j;
                else if (j == cur)
                    cur = i;
            }
        }
        priceSort(arr, start, cur);
        priceSort(arr, cur + 1, end);
    }
}