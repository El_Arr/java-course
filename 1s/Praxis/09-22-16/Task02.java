public class Task02{

	public static void main(String [] args) {
	
		int n = Integer.parseInt(args[0]);
		int [] a = new int[n];
		int i, max;
		
		for (i = 0; i < n; i++) {
			a[i] = Integer.parseInt(args[i+1]);
		}
		max = a[0];
		for (int x: a) {
			if (max < x) {
				max = x;
			}
		}	
		System.out.println(max);
	
	}
	
}