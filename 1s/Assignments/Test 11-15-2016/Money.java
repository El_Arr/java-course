/**
* @author Eldar Mingachev
* 11-601
* KR2, 001
*/

public class Money {
	private long r;
	private byte k;

	public Money() {
		this.r = 0;
		this.k = 0;
	}

	public Money(long r, byte k) {
		this.r = r;
		this.k = k;
	}

	public Money add(Money m) {
		Money result = new Money();
		result.r = (long) this.r + m.r;
		Byte k1 = new Byte(m.k);
		Byte tk = new Byte(this.k);
		Integer res = new Integer(0);
		res = tk.intValue() + k1.intValue();
		result.k = res.byteValue();
		if (result.k >= 100) {
			result.r += 1;
			tk = new Byte(result.k);
			res = tk.intValue() % 100;
			result.k = res.byteValue();
		}
		return result;
	}

	public Money sub(Money m) {
		Money result = new Money();
		result.r = (long) this.r - m.r;
		Byte k1 = new Byte(m.k);
		Byte tk = new Byte(this.k);
		Integer res = new Integer(0);
		res = tk.intValue() - k1.intValue();
		result.k = res.byteValue();
		return result;
	}

	public int div(Money m) {
		int result = 0;
		Long r1 = new Long(m.r);
		Byte k1 = new Byte(m.k);
		int a = r1.intValue()*100 + k1.intValue();
		Long r2 = new Long(this.r);
		Byte k2 = new Byte(this.k);
		int b = r2.intValue()*100 + k2.intValue();
		result = a / b;
		return result;
	}

	public Money divNum(double n) {
		Money result = new Money();
		Double r1 = this.r / n;
		result.r = r1.longValue();
		Double k1 = this.k / n;
		result.k = k1.byteValue();
		return result;
	}

	public Money multNum(double n) {
		Money result = new Money();
		Double r1 = this.r * n;
		result.r = r1.longValue();
		Double k1 = this.k * n;
		result.k = k1.byteValue();
		if (result.k >= 100) {
			Integer k2 = new Integer(result.k);
			Integer res = new Integer(k2.intValue() / 100);
			Long res1 = new Long(res.intValue());
			result.r += res1.longValue();
			Byte tk = new Byte(result.k);
			res = new Integer(tk.intValue() % 100);
			result.k = res.byteValue();
		}
		return result;		
	}

	public boolean equals(Money m) {
		if ((this.r == m.r) && (this.k == m.k)) {
			return true;
		}
		else
			return false;
	}

	public String toString() {
		return this.r + ", " + this.k;
	}

	public static void main() {
		Money m1 = new Money((long) 10, (byte) 20);
		Money m2 = new Money((long) 20, (byte) 10);
		Money m3 = m1.add(m2);
		System.out.println("Add (m3=m1+m2):" + "\n" + m3 + "\n");
		
		m3 = m1.sub(m2);
		System.out.println("Sub (m3=m1-m2):" + "\n" + m3 + "\n");
		
		m3 = m1.multNum(2);
		System.out.println("MultNum (m3=m1*2):" + "\n" + m3 + "\n");
		
		int n = m1.div(m2);
		System.out.println("Div (n=m1/m2):" + "\n" + n + "\n");
		
		m3.divNum(2.2);
		System.out.println("Div2 (m3/2.2):" + "\n" + m3 + "\n");
		
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				m1.r = 1;
				m1.k = 2;
				m2.r = 1;
				m2.k = 2;
				System.out.println("For r1 = r2 = 1, k1 = k2 = 2:");
			}
		
			if (m1.equals(m2)) {
				System.out.println("fr1 equals fr2");
			}
			else
				System.out.println("fr1 doesn't equal fr2" + "\n");
		}
	}
}