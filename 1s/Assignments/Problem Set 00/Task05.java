import java.util.Scanner;

public class Task05 {
	public static void main(String [] args) {
		
		Scanner in = new Scanner(System.in);

		System.out.println("Введите a b c");
		double a = in.nextInt();
		double b = in.nextInt();
		double c = in.nextInt();
		if (((a + b) > c) && ((b + c) > a) && ((a + c) > b)) {
			System.out.println("Существует");
		}
		else {
			System.out.println("Не существует");
		}
		
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 005
*/