import java.util.Scanner;

public class Task12{
	public static void main(String [] args){

		Scanner in = new Scanner(System.in);

		System.out.println("Введите n");
		int n =  in.nextInt();
		int sum = 0;
		n = Math.abs(n);
		while (n>0){
			sum += n%10;
			n = n/10;
		}
		System.out.println("Sum = " + sum);
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 012
*/	