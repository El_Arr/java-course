import java.util.Scanner;

public class Task17{
	public static void main(String [] args){

		Scanner in = new Scanner(System.in);

		System.out.println("Введите x");
		double x =  in.nextDouble();
		if (x==1) {
			System.out.println(x);
		}
		else {
			double x1=x*x;
			double x2=x1*x1;
			double x3=x2*x2;
			double x4=x3*x3;
			double x5=x4*x4;
			System.out.println("x^50 = " + x1*x4*x5);
		}
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 017
*/	