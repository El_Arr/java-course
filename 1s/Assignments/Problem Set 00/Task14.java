import java.util.Scanner;

public class Task14 {
	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);

		System.out.println("Введите N");
		int n = in.nextInt();
		int f = 10;
		int n1 = n;
		do {
			n1 = n1 / 10;
			f *= 10;
		}
		while (n1 > 0);
		n = f + n*10 + 1;
		System.out.println("1N1 = " + n);
		
	}	
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 014
*/	