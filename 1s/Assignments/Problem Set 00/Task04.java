import java.util.Scanner;

public class Task04{
	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);

		System.out.println("Введите x,y,z");
		double x =  in.nextInt();
		double y =  in.nextInt();
		double z =  in.nextInt();
		double min = ((x + y + z )/ 2) < (x * y * z) ? ((x + y + z) / 2)*((x + y + z) / 2) : (x * y * z) * (x * y * z); 
		System.out.println("Min = " + min);
		
	}
}

/**
* @author Eldar Mingachev
* 11-601
* 000, 004
*/	