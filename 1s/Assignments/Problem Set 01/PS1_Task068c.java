/**
* @author Eldar Mingachev
* 11-601
* 001, 068в
*/	

import java.util.Scanner;

public class PS1_Task068c{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		int n =  in.nextInt();
		int c = 0;
		int dt = n;
		boolean diff = true;
		while (dt > 0) {
			dt = dt/10;
			c++;
		}
		int [] d = new int[c];
		for (int i = 0; i < c; i++){
			d[i] = n%10;
			n = n/10;
		}
		for (int i = 0; ((i < c)&&(diff)); i++){
			for (int j = 0; j < c; j++){
				if ((d[i] == d[j])&&(i != j)) {
					diff = false;
				}
			}
		}
		if (diff){
			System.out.println("Верно");
		}
		else {
			System.out.println("Не верно");
		}
	}	
}