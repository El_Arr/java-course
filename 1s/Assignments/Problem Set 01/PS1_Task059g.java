/**
* @author Eldar Mingachev
* 11-601
* 001, 059ж
*/	

import java.util.Scanner;

public class PS1_Task059g{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите координаты точки (x;y)");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		if (( x <= (2- y)/2) && (x >= (y - 2)/2) && (y >= -1)){
			System.out.println("Принадлежит");
		}
		else
			System.out.println("Не принадлежит");
	
	}
	
}