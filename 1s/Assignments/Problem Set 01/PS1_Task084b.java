/**
* @author Eldar Mingachev
* 11-601
* 001, 084б
*/	

import java.util.Scanner;

public class PS1_Task084b{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите N и X");
		int n =  in.nextInt();
		double x =  in.nextDouble();
		double arg = 1;
		double res = 0;
		for (int i = 0; i < n; i++) {
			arg *= x;
			res += Math.sin(arg);
		}
		System.out.println("sin(x) + sin(x^2) + .. + sin(x^n)  = " + res);
	
	}

}