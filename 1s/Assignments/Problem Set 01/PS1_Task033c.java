/**
* @author Eldar Mingachev
* 11-601
* 001, 033в
*/	

import java.util.Scanner;

public class PS1_Task033c{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите X, Y");
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		double max,min;
		if (x>y) {
			max = x;
			min = y;
		}
		else {
			max = y;
			min = x;
		}
		System.out.println("Max = " + max);
		System.out.println("Min = " + min);
	}
	
}