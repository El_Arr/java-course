/**
* @author Eldar Mingachev
* 11-601
* 001, 068а
*/	

import java.util.Scanner;

public class PS1_Task068a{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		int n =  in.nextInt();
		int d = n;
		int c = 0;
		int fh = 0;
		int sh = 0;
		while (d > 0) {
			d = d/10;
			c++;
		}
		for (int i = 0; i < c; i++){
			d = n%10;
			if (c%2==0) {
				if (i < c/2) {
					sh += d*Math.pow(10,i );
				}
				else 
					if (i>=c/2){
						fh += d*Math.pow(10,c-i-1);
					}
			}
			else{
				if (i < c/2) {
					sh += d*Math.pow(10,i );
				}
				else 
					if (i>=c/2+1){
						fh += d*Math.pow(10,c-i-1);
					}				
			}
			n = n/10;
		}
		if (fh == sh){
			System.out.println("Палиндром");
		}
		else 
			if (fh != sh) {
				System.out.println("Не палиндром");
		}
	}
}