/**
* @author Eldar Mingachev
* 11-601
* 001, 077б
*/	

import java.util.Scanner;

public class PS1_Task077b{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		int n =  in.nextInt();
		int res = 1;
		for (int i = 0; i < n; i++){
			res *= i+1;
		}
		System.out.println(n + "! = " + res);
	}
}