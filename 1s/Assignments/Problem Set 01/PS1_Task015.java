import java.util.Scanner;

public class PS1_Task015{

	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Введите c b (c>m)");
		double c =  in.nextDouble();
		double b =  in.nextDouble();
		double a = Math.sqrt(c*c - b*b);
		double r = (a + b - c)/2;
		System.out.println("a = " + a);
		System.out.println("r = " + r);
		
	}
	
}

/**
* @author Eldar Mingachev
* 11-601
* 001, 015
*/	