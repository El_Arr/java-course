/**
* @author Eldar Mingachev
* 11-601
* 001, 082
*/	

import java.util.Scanner;

public class PS1_Task082{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите x");
		double x =  in.nextDouble();
		double ch = 1;
		double zn = 1;
		for (double i = 1; i <= 64; i++){
			if (i%2 == 1) {
				zn *= (x - i);
			}
			else
				if (i%2 == 0) {
					ch *= (x - i);
				}
		}
		System.out.println("Результат = " + ch/zn);
		
	}
	
}