/**
* @author Eldar Mingachev
* 11-601
* 001, 077г
*/	

import java.util.Scanner;

public class PS1_Task077d{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		double n =  in.nextInt();
		double res = 0;
		double c = 0;
		for (double i = 0; i < n; i++){
			c += Math.sin(i+1);
			res += 1/c;
		}
		System.out.println("Результат = " + res);
	}
}