/**
* @author Eldar Mingachev
* 11-601
* 001, 077е
*/	

import java.util.Scanner;

public class PS1_Task077f{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		double n =  in.nextInt();
		double res = 1;
		double zn = 0;
		double ch = 0;
		for (double i = 0; i < n; i++){
			ch += Math.cos(i+1);
			zn += Math.sin(i+1);
			res *= ch/zn;
		}
		System.out.println("Результат = " + res);
	}
}