/**
* @author Eldar Mingachev
* 11-601
* 001, 114e
*/	

public class PS1_Task114f{

	public static void main(String [] args) {

		double res = 1;
		double fact = 1;
		for (double i = 1; i <=10; i++) {
			fact *= i;
			res *= (2 + (1/fact));
		}
		System.out.println("Результат = " + res);
		
	}
	
}