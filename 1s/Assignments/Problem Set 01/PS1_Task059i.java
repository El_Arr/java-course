/**
* @author Eldar Mingachev
* 11-601
* 001, 059и
*/	

import java.util.Scanner;

public class PS1_Task059i{

	public static void main(String [] args) {

		Scanner in = new Scanner(System.in);
		System.out.println("Введите координаты точки (x;y)");
		while (1 == 1){
		double x =  in.nextDouble();
		double y =  in.nextDouble();
		if ((((y >= 0) && ((y <= (-1 * x)) || ((x >= 0) && (x <= 1) && (y == 0))))|| ((y < 0) && (y >= (x - 1)/3))) && (y <= (2 * x + 3))){
			System.out.println("Принадлежит");
		}
		else {
			System.out.println("Не принадлежит");
		}
		}
	}
	
}

/*
(1;0)
(-2;-1)
y = (x - 1) / 3
(-1;1)
(-2;-1)
y = 2 * x + 3
*/