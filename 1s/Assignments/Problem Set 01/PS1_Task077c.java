/**
* @author Eldar Mingachev
* 11-601
* 001, 077в
*/	

import java.util.Scanner;

public class PS1_Task077c{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите n");
		double n =  in.nextInt();
		double res = 1;
		double cur;
		for (double i = 0; i < n; i++){
			cur = 1 + (1/((i+1)*(i+1)));
			res *= cur;
		}
		System.out.println("Результат = " + res);
	}
}