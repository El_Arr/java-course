/**
* @author Eldar Mingachev
* 11-601
* 001, 078в
*/	

import java.util.Scanner;

public class PS1_Task078c{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите a");
		int a =  in.nextInt();
		System.out.println("Введите n");
		int n =  in.nextInt();
		double res = 0;
		double c = 1;
		for (double i = 0; i < n; i++){
			c *= a+i;
			res += 1/c;
		}
		System.out.println("Результат = " + res);
	}
}