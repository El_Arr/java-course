package assignments;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task05 {

    public static void main(String[] args) {
        final int N = 10;
        int countM = 0;
        int countF = 0;
        int count = 0;
        int i = 0;
        Random r = new Random(0);
        String[] match = new String[N];
        String[] find = new String[N];
        String temp;
        Matcher m;
        Pattern forFinding = Pattern.compile("[02468]{3}");
        Pattern forMatching = Pattern.compile("\\d*" + forFinding + "\\d*");
        while (match[N - 1] == null && find[N - 1] == null) {
            temp = String.valueOf(Math.abs(r.nextInt()));
            m = forMatching.matcher(temp);
            count++;
            if (!m.matches()) {
                match[countM] = temp;
                countM++;
            }
            m = forFinding.matcher(temp);
            i = 0;
            if (!m.find()) {
                find[countF] = temp;
                countF++;
            }
        }
        System.out.println("Числа, удовлетворяющие регулярному выражению:");
        System.out.println("Matches:");
        for (i = 0; i < countM; i++) {
            System.out.println(match[i]);
        }
        System.out.println("\n" + "Find:");
        for (i = 0; i < countF; i++) {
            System.out.println(find[i]);
        }
        System.out.println("Общее количество чисел = " + count);
    }
}