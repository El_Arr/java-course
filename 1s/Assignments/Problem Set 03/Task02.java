package assignments;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task02 {

    public static void main(String[] args) {
        String[] a = new String[]{"0", "-6", "-0.5", "+2", "0,0(64)", "-0", "001", "0(35)00", "-3,750"};
        boolean[] b = new boolean[a.length];
        Pattern p = Pattern.compile("((\\+|-)?((([1-9]\\d*)((\\.|,)\\d*(\\(\\d*[1-9]\\))|[1-9])?$)|(0(\\.|,)\\d*((\\(\\d*[1-9]\\))|[1-9]))))|0");
        Matcher m;
        for (int i = 0; i < a.length; i++) {
            m = p.matcher(a[i]);
            if (m.matches()) {
                b[i] = true;
            } else
                b[i] = false;
        }
        System.out.println("Строки, подходят регулярному выражению?");
        for (int i = 0; i < a.length; i++) {
            System.out.println(b[i]);
        }
    }
}