package assignments;

import java.util.Random;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Task06 {

    public static void main(String[] args) {
        final int N = 10;
        int countM = 0;
        int countF = 0;
        int count = 0;
        int c = 0;
        Random r = new Random(0);
        String[] match = new String[N];
        String[] find = new String[N];
        String temp;
        Matcher m;
        Matcher m1;
        Pattern forFinding = Pattern.compile("[02468]");
        Pattern forFinding1 = Pattern.compile("[13579]");
        Pattern forMatching = Pattern.compile(forFinding + "{4,5}");
        while (match[N - 1] == null && find[N - 1] == null) {
            temp = String.valueOf(Math.abs(r.nextInt()));
            m = forMatching.matcher(temp);
            c++;
            if (m.matches()) {
                match[countM] = temp;
                countM++;
            }
            m = forFinding.matcher(temp);
            m1 = forFinding1.matcher(temp);
            while (m.find() && !m1.find()) {
                count++;
            }
            if (count > 3 && count < 6) {
                find[countF] = temp;
                countF++;
            }
            count = 0;
        }
        System.out.println("Числа, удовлетворяющие регулярному выражению:");
        System.out.println("Matches:");
        for (int i = 0; i < countM; i++) {
            System.out.println(match[i]);
        }
        System.out.println("\n" + "Find:");
        for (int i = 0; i < countF; i++) {
            System.out.println(find[i]);
        }
        System.out.println("Общее количество чисел = " + c);
    }
}