/**
* @author Eldar Mingachev
* 11-601
* HW 19.11.2016, Task 3
*/

import java.util.Scanner;

public class Game {
	private static Scanner in = new Scanner(System.in);
	private static Player p1 = new Player();
	private static Player p2 = new Player();
	private static boolean firstHits = true;
	private static boolean winnerFirst = false;

	public static boolean playing() {
		if ((p1.getHP() <= 0) || (p2.getHP() <= 0)) {
				if (p1.getHP() <= 0) {
					winnerFirst = false;
				}
				else 
					if (p2.getHP() <= 0) {
						winnerFirst = true;
					}
				return false;
			}
		else {
			return true;
		}
	}

	public static void kick() {
		Player hit, got;
		String n;
		int power;
		if (firstHits) {
			hit = p1;
			got = p2;
			firstHits = false;
		}
		else {
			hit = p2;
			got = p1;
			firstHits = true;
		}
		System.out.println(hit.getName() + " hits with the force of:");
		power = in.nextInt();
		hit.kick(got, power);
		System.out.println(got.getName() + "'s HP now  is " + got.getHP());			
	}

	public static void main(String[] args) {
		
		System.out.println("Welcome to Kickgame." + "\n" 
			+ "Hit your enemy, by entering the power in range from 1 to 10, and win!" + "\n"
			+ "Remember, younlings: The force, the greater is -  the greater is the chance to miss.");
		
		System.out.println("Enter the name of Player 1");
		p1.setName(in.nextLine());

		System.out.println("Enter the name of Player 2");
		p2.setName(in.nextLine());
		
		while (playing()) {
			kick();
		}

		Player winner;
		if (winnerFirst) {
			winner = p1;
		}
		else {
			winner = p2;
		}
		System.out.println(winner.getName() + " wins!");	
	}
}