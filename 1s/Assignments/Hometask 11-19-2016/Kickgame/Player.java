/**
* @author Eldar Mingachev
* 11-601
* HW 19.11.2016, Task 3
*/

public class Player {

	private int hp;
	private String name;
	
	public Player() {
		hp = 20;
		this.name = ""; 
	}

	public Player(String name) {
		hp = 20;
		this.name = name; 
	}

	public void kick(Player p, int power) {
		final int BORDER = 10;
		int r = (int) (Math.random() * BORDER);
		int f = BORDER + power % 10  - power;
		if (r <= f) {
			p.decHP(power); 
			System.out.println("GOTCHA!" + "\n");
		}
		else {
			System.out.println("MISSED!" + "\n");
		}
	}

	public void decHP(int d) {
		this.hp -= d;
	}

	public int getHP() {
		return this.hp;
	}

	public void setName(String n) {
		this.name = n;
	}

	public String getName() {
		return this.name;
	}

}