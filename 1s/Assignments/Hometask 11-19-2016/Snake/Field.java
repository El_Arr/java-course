/**
* @author Eldar Mingachev
* 11-601
* HW 19.11.2016, Task 4
*/

public class Field {
	private final int SIZE = 10;
	private char [][] field = new char[SIZE][SIZE];

	public Field() {
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				if ((i == 0) || (i == SIZE - 1)) {
					field[i][j] = '-';
				}
				else {
					if ((j == 0) || (j == SIZE - 1)) {
						field[i][j] = '|';
					}
					else {
						field[i][j] = ' ';
					}	
				}
			}
		}
	}

	public void render(Snake s) {
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				for (int k = 0; k < s.getLength(); k++) {
					if ((i == s.getCell(k).getX()) && (j == s.getCell(k).getY())) {
						field[i][j] = '*';
					}
				}
			}
		}
	}

	public void render(Food f) {
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				if ((i == f.getX()) && (j == f.getY())) {
					field[i][j] = 'x';
				}
			}
		}
	}

	public void show() {
		for (int i = 0; i < SIZE; i++) {
			for (int j = 0; j < SIZE; j++) {
				System.out.print(field[i][j]);
			}
			System.out.println();
		}
	}
}