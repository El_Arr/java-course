/**
* @author Eldar Mingachev
* 11-601
* HW 19.11.2016, Task 4
*/

import java.util.Random;

public class Food {
	private int x,y;

	public Food(Snake s) {
		Random r = new Random();
		boolean check = true;
		int x0 = -1;
		int y0 = -1;
		while (check) {
			x0 = r.nextInt(10);
			y0 = r.nextInt(10);
			check = false;
			for (int i = 0; ((i < s.getLength()) && (!check)) ; i++) {
				if ((x0 == s.getCell(i).getX()) && (y0 == s.getCell(i).getY())) {
					check = true;
				}
			}
		}
		this.x = x0;
		this.y = y0;
	}

	public int getX() {
		return this.x;
	}

	public int getY() {
		return this.y;
	}

}