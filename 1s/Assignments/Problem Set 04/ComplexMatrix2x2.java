/**
* @author Eldar Mingachev
* 11-601
* 004, 009
*/

package ru.kpfu.itis.el_arr.util;

public class ComplexMatrix2x2 {
	private ComplexNumber [][] matrix = new ComplexNumber[2][2];
	
	public ComplexMatrix2x2() {
		ComplexNumber a = new ComplexNumber();
		matrix[0][0] = a;
		matrix[0][1] = a;
		matrix[1][0] = a;
		matrix[1][1] = a;

	}

	public ComplexMatrix2x2(ComplexNumber a) {
		matrix[0][0] = a;
		matrix[0][1] = a;
		matrix[1][0] = a;
		matrix[1][1] = a;

	}
	
	public ComplexMatrix2x2(ComplexNumber a, ComplexNumber b, ComplexNumber c, ComplexNumber d) {
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}

	public void setE(int i, int j, ComplexNumber cn) {
		this.matrix[i][j] = cn;
	}

	public ComplexNumber getE(int i,int j) {
		return this.matrix[i][j];
	}

	public ComplexMatrix2x2 add(ComplexMatrix2x2 cm) {
		ComplexMatrix2x2 result = new ComplexMatrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result.matrix[i][j] = this.matrix[i][j].add(cm.matrix[i][j]);
			}
		}
		return result;
	}
	
	public ComplexMatrix2x2 mult(ComplexMatrix2x2 m) {
		ComplexMatrix2x2 result = new ComplexMatrix2x2();
		ComplexNumber c = new ComplexNumber();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				c.setA(0);
				c.setB(0);
				for (int k = 0; k < matrix[i].length; k++) {
					c.add2(matrix[i][k].mult(m.matrix[k][j]));
				}
				result.matrix[i][j] = c;
			}
		}
		return result;
	}

	public ComplexNumber det() {
		return (matrix[0][0].mult(matrix[1][1])).sub(matrix[0][1].mult(matrix[1][0]));
	}

	public ComplexVector2D multVector(ComplexVector2D cv) {
		ComplexVector2D result = new ComplexVector2D();
		ComplexNumber [][] matrix = this.matrix;
		ComplexNumber c = new ComplexNumber();
		ComplexNumber t = new ComplexNumber();
		for (int i = 0; i < matrix.length; i++) {
			c.setA(0);
			c.setB(0);
			for (int j = 0; j < matrix[i].length; j++) {
				if (j % 2 == 0) {
					t = cv.getX();
				}
				else 
					if (j % 2 == 1) {
						t = cv.getY();
					}
				c.add2(matrix[i][j].mult(t));
			}
			if (i % 2 == 0) {
				result.setX(c);
			}
			else 
				if (i % 2 == 1) {
					result.setY(c);
				} 
		}
		return result;
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				str += matrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}

	public static void main() {
		ComplexNumber a11 = new ComplexNumber(1,2);
		ComplexNumber a12 = new ComplexNumber(3,4);
		ComplexNumber a21 = new ComplexNumber(5,6);
		ComplexNumber a22 = new ComplexNumber(7,8);
		ComplexMatrix2x2 cm1 = new ComplexMatrix2x2(a11,a12,a21,a22);
		System.out.println("RationalMatrix1: " + "\n" + cm1);

		ComplexNumber a = new ComplexNumber(1,2);
		ComplexMatrix2x2 cm2 = new ComplexMatrix2x2(a);
		System.out.println("\n" + "RationalMatrix2: " + "\n" + cm2);

		ComplexMatrix2x2 cm3 = cm1.add(cm2);
		System.out.println("\n" + "cm1 add cm2:" + "\n" + cm3);
		
		cm3 = cm1.mult(cm2);
		System.out.println("\n" + "cm1 mult cm2: " + "\n" + cm3);
		
		System.out.println("\n" + "det cm3: " + "\n" + cm3.det());
		
		ComplexNumber x = new ComplexNumber(1,5);
		ComplexNumber y = new ComplexNumber(1,5);
		ComplexVector2D cv1 = new ComplexVector2D(x,y);
		ComplexVector2D cv2 = cm1.multVector(cv1);
		System.out.println("m1 multVector cv1(1 + 5i;1 + 5i):" + "\n" + cv2 + "\n");
	}
}