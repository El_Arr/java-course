/**
* @author Eldar Mingachev
* 11-601
* 004, 012
*/

package ru.kpfu.itis.el_arr.util;

public class RationalComplexMatrix2x2 {
		private RationalComplexNumber [][] matrix = new RationalComplexNumber[2][2];
	
	public RationalComplexMatrix2x2() {
		RationalComplexNumber a = new RationalComplexNumber();
		matrix[0][0] = a;
		matrix[0][1] = a;
		matrix[1][0] = a;
		matrix[1][1] = a;
	}

	public RationalComplexMatrix2x2(RationalComplexNumber a) {
		matrix[0][0] = a;
		matrix[0][1] = a;
		matrix[1][0] = a;
		matrix[1][1] = a;
	}

	public RationalComplexMatrix2x2(RationalComplexNumber a, RationalComplexNumber b, RationalComplexNumber c, RationalComplexNumber d) {
		matrix[0][0] = a;
		matrix[0][1] = b;
		matrix[1][0] = c;
		matrix[1][1] = d;
	}

	public RationalComplexMatrix2x2 add(RationalComplexMatrix2x2 rcm) {
		RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				result.matrix[i][j] = this.matrix[i][j].add(rcm.matrix[i][j]);
			}
		}
		return result;
	}

	public RationalComplexMatrix2x2 mult(RationalComplexMatrix2x2 rcm) {
		RationalComplexMatrix2x2 result = new RationalComplexMatrix2x2();
		RationalComplexNumber c = new RationalComplexNumber();
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				c = new RationalComplexNumber();
				for (int k = 0; k < matrix[i].length; k++) {
					c = c.add(matrix[i][k].mult(rcm.matrix[k][j]));
				}
				result.matrix[i][j] = c;
			}
		}
		return result;
	}

	public String toString() {
		String str = "";
		for (int i = 0; i < matrix.length; i++) {
			for (int j = 0; j < matrix[i].length; j++) {
				str += matrix[i][j] + " ";
			}
			str += "\n";
		}
		return str;
	}

	public RationalComplexNumber det() {
		return (matrix[0][0].mult(matrix[1][1])).sub(matrix[0][1].mult(matrix[1][0]));
	}

	public RationalComplexVector2D multVector(RationalComplexVector2D rcv) {
		RationalComplexVector2D result = new RationalComplexVector2D();
		RationalComplexNumber [][] matrix = this.matrix;
		RationalComplexNumber c = new RationalComplexNumber();
		RationalComplexNumber t = new RationalComplexNumber();
		RationalComplexNumber nv;
		for (int i = 0; i < matrix.length; i++) {
			c = new RationalComplexNumber();
			for (int j = 0; j < matrix[i].length; j++) {
				if (j % 2 == 0) {
					t = rcv.getX();
				}
				else 
					if (j % 2 == 1) {
						t = rcv.getY();
					}
				c = c.add(matrix[i][j].mult(t));
			}
			if (i % 2 == 0) {
				nv = new RationalComplexNumber(c.getA(),c.getB());
				result.setX(nv);
			}
			else 
				if (i % 2 == 1) {
					nv = new RationalComplexNumber(c.getA(),c.getB());
					result.setY(nv);
			} 
		}
		return result;
	}

	public static void main() {
		RationalFraction ax1 = new RationalFraction(1,2);
		RationalFraction bx1 = new RationalFraction(3,4);
		RationalComplexNumber x1 = new RationalComplexNumber(ax1,bx1);
		
		RationalFraction ay1 = new RationalFraction(5,6);
		RationalFraction by1 = new RationalFraction(7,8);
		RationalComplexNumber y1 = new RationalComplexNumber(ay1,by1);
		
		
		RationalFraction ax2 = new RationalFraction(5,6);
		RationalFraction bx2 = new RationalFraction(7,8);
		RationalComplexNumber x2 = new RationalComplexNumber(ax2,bx2);
		
		RationalFraction ay2 = new RationalFraction(9,10);
		RationalFraction by2 = new RationalFraction(11,12);
		RationalComplexNumber y2 = new RationalComplexNumber(ay2,by2);		

		RationalComplexMatrix2x2 rcm1 = new RationalComplexMatrix2x2(x1,y1,x2,y2);
		System.out.println("RCM1:" + "\n" + rcm1 + "\n");

		RationalFraction ax = new RationalFraction(1,2);
		RationalFraction bx = new RationalFraction(3,4);
		RationalComplexNumber x = new RationalComplexNumber(ax,bx);
		
		RationalComplexMatrix2x2 rcm2 = new RationalComplexMatrix2x2(x);		
		System.out.println("RCM2:" + "\n" + rcm2 + "\n");
		
		RationalComplexMatrix2x2 rcm3 = rcm1.add(rcm2);
		System.out.println("Add:" + "\n" + rcm3 + "\n");

		rcm3 = rcm1.mult(rcm2);
		System.out.println("Mult:" + "\n" + rcm3 + "\n");
		
		System.out.println("Det rcm1:" + "\n" + rcm1.det() + "\n");
		
		RationalComplexVector2D rcv = new RationalComplexVector2D(x1,y1);
		rcv = rcm1.multVector(rcv);
		System.out.println("multVector:" + "\n" + rcv + "\n");
	}
}