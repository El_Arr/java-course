/**
* @author Eldar Mingachev
* 11-601
* 004, 001
*/

import java.util.Random;

public class Teacher {
	private String name;
	private String subject;
	private Random r = new Random();

	public Teacher() {
	}

	public Teacher(String name, String subject) {
		this.name = name;
		this.subject = subject;
	}
	
	public void setName(String name) {
		this.name = name;
	}

	public String getName() {
		return name;
	}

	public void setSubject(String subject) {
		this.subject = subject;
	}

	public String getSubject() {
		return subject;
	}

	public void classifyStudent(Student student) {
		int m = r.nextInt(5);
		m += 2;
		String mark = "";
		if (m == 2) {
			mark = "неудовлетворительно";
		}
		else 
			if (m == 3) {
				mark = "удовлетворительно";
			}
			else
				if (m == 4) {
					mark = "хорошо";
				}
				else
					if (m >= 5) {
						mark = "отлично";
					}
		System.out.println("Преподаватель " + name + " оценил знания студента с именем " + student.getName() + " по предмету " + subject + " на оценку " + mark);
	}

}