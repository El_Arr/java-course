/**
* @author Eldar Mingachev
* 11-601
* 004, 002
*/

package ru.kpfu.itis.el_arr.util;

public class Vector2D {
	private double x,y;
		
	public Vector2D() {
		this.x = 0;
		this.y = 0;
	}

	public Vector2D(double x, double y) {
		this.x = x;
		this.y = y;
	}

	public void setX(double x) {
		this.x = x;
	}

	public double getX() {
		return x;
	}

	public void setY(double y) {
		this.y = y;
	}

	public double getY() {
		return y;
	}

	public Vector2D add(Vector2D v) {
		Vector2D result = new Vector2D((x + v.getX()), (y + v.getY()));
		return result;
	}

	public void add2(Vector2D v) {
		this.x += v.getX();
		this.y += v.getY();
	}

	public Vector2D sub(Vector2D v) {
		Vector2D result = new Vector2D((x - v.getX()), (y - v.getY()));
		return result;
	}

	public void sub2(Vector2D v) {
		this.x -= v.getX();
		this.y -= v.getY();
	}

	public Vector2D mult(double t) {
		Vector2D result = new Vector2D((this.x * t), (this.y * t));
		return result;
	}

	public void mult2(double t) {
		this.x *= t;
		this.y *= t;
	}

	public double length() {
		double result = Math.sqrt(x*x + y*y);
		return result;
	}

	public double scalarProduct(Vector2D v) {
		return ((x * v.getX()) + (y * v.getY()));
	}

	public double cos(Vector2D v) {
		Vector2D v0 = new Vector2D(this.x, this.y);
		double result = v0.scalarProduct(v) / (v0.length() * v.length());
		return result;
	}

	public boolean equals(Vector2D v) {
		if ((x == v.getX()) && (y == v.getY())) {
			return true;
		}
		else 
			return false;
	}

	public String toString() {
		return "<" + x + ", " + y + ">";
	}

	public static void main() {
		Vector2D v1 = new Vector2D();
		v1.setX(1);
		v1.setY(2);
		
		Vector2D v2 = new Vector2D();
		v2.setX(3);
		v2.setY(4);

		Vector2D v3 = v1.add(v2);
		System.out.println("Add:" + "\n" + v3 + "\n");
		
		v3.add2(v2);
		System.out.println("Add2:" + "\n" + v3 + "\n");
		
		v3 = v1.sub(v2);
		System.out.println("Sub:" + "\n" + v3 + "\n");
		
		v3.sub2(v2);
		System.out.println("Sub2:" + "\n" + v3 + "\n");
		
		v3 = v1.mult(2);
		System.out.println("Mult (v1*2):" + "\n" + v3 + "\n");
		
		v3.mult2(2);
		System.out.println("Mult2:" + "\n" + v3 + "\n");
		
		System.out.println("Length of v3:" + "\n" + v3.length() + "\n");
		
		System.out.println("ScalarProduct(v1, v2):" + "\n" + v1.scalarProduct(v2) + "\n");
		
		System.out.println("Cos(v1^v2):" + "\n" + v1.cos(v2) + "\n");	
		
		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				v1.setX(1);
				v1.setY(2);
				v2.setX(1);
				v2.setY(2);
				System.out.println("For x1 = x2 = 1, y1 = y2 = 2:");
			}
		
			if (v1.equals(v2)) {
				System.out.println("v1 equals v2");
			}
			else
				System.out.println("v1 doesn't equal v2" + "\n");
		}
	}
}