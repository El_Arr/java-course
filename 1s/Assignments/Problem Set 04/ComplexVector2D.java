/**
* @author Eldar Mingachev
* 11-601
* 004, 007
*/

package ru.kpfu.itis.el_arr.util;

public class ComplexVector2D {
	private ComplexNumber x = new ComplexNumber();
	private ComplexNumber y = new ComplexNumber();

	public ComplexVector2D() {
	}

	public ComplexVector2D(ComplexNumber x, ComplexNumber y) {
		this.x = x;
		this.y = y;
	}

	public void setX(ComplexNumber x) {
		this.x = x;
	}

	public ComplexNumber getX() {
		return this.x;
	}

	public void setY(ComplexNumber y) {
		this.y = y;		
	}

	public ComplexNumber getY() {
		return this.y;
	}	

	public ComplexVector2D add(ComplexVector2D cv) {
		ComplexVector2D result = new ComplexVector2D();
		result.x = this.x.add(cv.x);
		result.y = this.y.add(cv.y);
		return result;
	}

	public ComplexNumber scalarProduct(ComplexVector2D cv) {
		return (this.x.mult(cv.x)).add(this.y.mult(cv.y));
	}

	public boolean equals(ComplexVector2D cv) {
		if ((this.x.equals(cv.x)) && (this.y.equals(cv.y))) {
			return true;
		}
		else 
			return false;
	}

	public String toString() {
		return "<" + x + ", " + y + ">";
	}

	public static void main() {
		ComplexNumber x1 = new ComplexNumber(2,3);
		ComplexNumber y1 = new ComplexNumber(4,5);
		ComplexVector2D cv1 = new ComplexVector2D(x1,y1);
		System.out.println("Cv1: " + "\n" + cv1);

		ComplexNumber x2 = new ComplexNumber(1,2);
		ComplexNumber y2 = new ComplexNumber(3,4);
		ComplexVector2D cv2 = new ComplexVector2D(x2,y2);
		System.out.println("\n" + "Cv2: " + "\n" + cv2);

		ComplexVector2D cv3 = cv1.add(cv2);
		System.out.println("\n" + "Cv1 add Cv2: " + "\n" + cv3);

		ComplexNumber cn = cv1.scalarProduct(cv2);
		System.out.println("\n" + "ScalarProduct: " + "\n" + cn);

		for (int i = 0; i < 2; i++) {
			if (i == 1) {
				cv1.setX(x1);
				cv1.setY(y1);
				cv2.setX(x1);
				cv2.setY(y1);
				System.out.println("For x1 = x2 = 2 + 3*i, y1 = y2 = 4 + 5*i:");
			}
		
			if (cv1.equals(cv2)) {
				System.out.println("v1 equals v2");
			}
			else
				System.out.println("\n" + "v1 doesn't equal v2" + "\n");
		}
	}
}