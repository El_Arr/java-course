package assignments;

public class RationalFrac {
    private int p;
    private int q;
    private int d = 0;

    public RationalFrac(int p, int q) {
        this(0, p, q);
    }

    public RationalFrac(int d, int p, int q) {
        this.p = p;
        this.q = q;
        this.d = d;
        makeItRight();
    }

    public void makeItRight() {
        if (p > q) {
            int temp = p / q;
            p -= d * q;
            d = temp;
        }
    }

    public RationalFrac sum(RationalFrac rf) {
        RationalFrac res;
        RationalFrac t1 = new RationalFrac(0, this.p + this.q * this.d, this.q);
        RationalFrac t2 = new RationalFrac(0, rf.p + rf.q * rf.d, rf.q);
        if (this.q == rf.q) {
            res = new RationalFrac(t1.p + t2.p, t1.q);
        } else {
            res = new RationalFrac(t1.p * t2.q + t2.p * t1.q, t2.q * t1.q);
        }
        res.makeItRight();
        if (res.p == res.q) {
            res.p = 0;
            res.d++;
        }
        return res;
    }

    public RationalFrac mult(RationalFrac rf) {
        RationalFrac t1 = new RationalFrac(0, this.p + this.q * this.d, this.q);
        RationalFrac t2 = new RationalFrac(0, rf.p + rf.q * rf.d, rf.q);
        RationalFrac res = new RationalFrac(t1.p * t2.p, t1.q * t2.q);
        res.makeItRight();
        return res;
    }

    public void sout() {
        System.out.println(this);
    }

    public String toString() {
        if (d == 0) {
            if (p == 0) {
                return "0";
            } else
                return "" + p + "/" + q;
        } else if (p == 0) {
            return "" + d;
        } else
            return "" + d + " " + p + "/" + q;
    }

}
