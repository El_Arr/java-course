/**
* @author Eldar Mingachev
* 11-601
* KR1, Task 3Б
*/	

import java.util.Scanner;

public class Task3B{
	
	public static void main(String [] args){
		
		Scanner in = new Scanner(System.in);
		
		System.out.println("Введите N");
		int n = in.nextInt();
		int ct = 0;
		int i,j;
		int p;
		int [][] a = new int[n][n];
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				a[i][j] = 0;
			}
		}
		System.out.println("Вывод");
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				System.out.print(a[i][j] + " ");
			}
			System.out.println();
		}
		
		System.out.println("Введите элементы массива");
		for (i = 0; i < n; i++) {
			for (j = 0; j < i+1; j++) {
				a[i][j] = in.nextInt();
				if ((j+=ct) <= n){
					j+=ct;
				}
				if ((i-=ct) <= n){
					i-=ct;
				}
			}
			ct++;
		}
		System.out.println("Вывод");
		for (i = 0; i < n; i++) {
			for (j = 0; j < n; j++) {
				System.out.print(a[j][i] + " ");
			}
				System.out.println();
		}
	}
}