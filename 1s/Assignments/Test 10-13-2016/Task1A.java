/**
* @author Eldar Mingachev
* 11-601
* KR1, Task1A
*/	

import java.util.Scanner;

public class Task1A{
	public static void main(String [] args){
		Scanner in = new Scanner(System.in);
		System.out.println("Введите X");
		double x =  in.nextDouble();
		double eps = 1E-9;
		double pr;
		double cur = 1;
		double ch = x;
		double zn = 1;
		double res = 0;
		int i = 0;
		int c = 1;
		do {
			i++;
			c *= -1;
			ch *= x*x;
			zn *=(2*i+1);
			cur = (c*ch)/zn;
			res += cur;
			pr = cur;
		}
		while (Math.abs(pr - cur) > eps);
		System.out.println("sinX = "+ res);
		System.out.println("Math.sin = " + Math.sin(x));
	}
}