package ru.kpfu.itis.god_and_preacher.client;

import ru.kpfu.itis.god_and_preacher.model.Table;
import ru.kpfu.itis.god_and_preacher.model.player.entities.Player;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerGod;
import ru.kpfu.itis.god_and_preacher.util.ObjectExchanger;

import java.net.Socket;

public class Client {
    private Socket socket;
    private Player player;

    public Client(Socket socket) {
        this.socket = socket;
    }

    public Client(Socket socket, Player player) {
        this.socket = socket;
        this.player = player;
    }

    public void init(String username, String rule) {
        Object object = ObjectExchanger.get(socket);
        if (object instanceof Player) {
            ((Player) object).setUsername(username);
            if (object instanceof PlayerGod) {
                ((PlayerGod) object).setWorldRule(rule);
            }
            this.player = ((Player) object);
            ObjectExchanger.send(socket, player);
        } else {
            System.out.println(object);
        }
    }

    public void start() {
        Object object = ObjectExchanger.get(socket);
        if (object instanceof Table) {
            this.player.setTable((Table) object);
            ObjectExchanger.send(
                    socket,
                    this.player.getPlayerBehaviour().behave(
                            this.player.getTable()
                    )
            );
        } else {
            System.out.println(object);
        }
    }

    public Socket getSocket() {
        return socket;
    }

    public void setPlayer(Player player) {
        this.player = player;
    }

    public Player getPlayer() {
        return player;
    }

}
