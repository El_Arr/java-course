package ru.kpfu.itis.god_and_preacher.data.repository;

import ru.kpfu.itis.god_and_preacher.model.Card;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;

public class CardRepositoryImpl implements Repository<Card> {

    public List<Card> getByQuery(String query) {
        List<Card> list = new ArrayList<>();
        try {
            PreparedStatement st = conn.prepareStatement(query);
            ResultSet rs = st.executeQuery();
            while (rs.next()) {
                list.add(
                        new Card(
                                rs.getString("suit"),
                                rs.getString("type")
                        )
                );
            }
            return list;
        } catch (SQLException e) {
            e.printStackTrace();
        }
        return null;
    }

}
