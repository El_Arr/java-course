package ru.kpfu.itis.god_and_preacher.model.player.entities;

import ru.kpfu.itis.god_and_preacher.model.player.behaviour.PlayerGodBehaviour;
import ru.kpfu.itis.god_and_preacher.server.Server;

public class PlayerGod extends Player {
    private String worldRule;

    public PlayerGod(String username, String godRule) {
        super(username);
        this.playerBehaviour = new PlayerGodBehaviour(this);
        this.worldRule = godRule;
        this.god = true;
    }

    public String getWorldRule() {
        return worldRule;
    }

    public void setWorldRule(String worldRule) {
        this.worldRule = worldRule;
    }
}
