package ru.kpfu.itis.god_and_preacher.data.repository;

import ru.kpfu.itis.god_and_preacher.data.database.Connection;

import java.util.List;

public interface Repository<T> {
    java.sql.Connection conn = Connection.getConnection();

    List<T> getByQuery(String query);

}
