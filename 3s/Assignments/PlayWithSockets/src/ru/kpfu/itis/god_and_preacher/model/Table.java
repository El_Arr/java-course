package ru.kpfu.itis.god_and_preacher.model;

import ru.kpfu.itis.god_and_preacher.data.service.CardService;
import ru.kpfu.itis.god_and_preacher.model.player.entities.Player;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerGod;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerOrdinary;
import ru.kpfu.itis.god_and_preacher.model.player.entities.PlayerPreacher;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

public class Table implements Serializable {
    private List<Card> tableCards;
    private Card offeredCard;
    private List<Player> players;
    private PlayerPreacher preacher;
    private PlayerGod god;

    private void distributeDeck(List<Card> deck) {
        if (deck != null && !deck.isEmpty() && players != null && !players.isEmpty()) {
            int handSize = tableCards.size() / (players.size() - 1);
            for (Player p : players) {
                if (p instanceof PlayerOrdinary) {
                    for (int i = 0; i < handSize; i++) {
                        ((PlayerOrdinary) p).getHand().addCard(deck.get(i));
                        deck.remove(i);
                    }
                }
            }
        }
    }

    public Table(List<Player> players) {
        this.players = players;
        this.tableCards = new ArrayList<>();
        List<Card> allCards = CardService.getAll();
        Collections.shuffle(allCards);
        distributeDeck(allCards);
    }

    public int getTableCardsCount() {
        return this.tableCards.size();
    }

    public List<Card> getTableCards() {
        return tableCards;
    }

    public void putOfferedCardOnTable() {
        this.tableCards.add(offeredCard);
    }

    public void offerCard(Card card) {
        this.offeredCard = card;
    }

    public Card getOfferedCard() {
        return offeredCard;
    }

    public List<Player> getPlayers() {
        return players;
    }

    public void addPlayer(Player player) {
        players.add(player);
    }

    public void setPreacher(PlayerPreacher preacher) {
        this.preacher = preacher;
    }

    public PlayerPreacher getPreacher() {
        return preacher;
    }

    public void setGod(PlayerGod god) {
        this.god = god;
    }

    public PlayerGod getGod() {
        return god;
    }
}
