package ru.kpfu.itis.god_and_preacher.model.player.behaviour;

import ru.kpfu.itis.god_and_preacher.model.Table;

import java.util.Scanner;

public interface PlayerBehaviour {
    Scanner in = new Scanner(System.in);

    Table behave(Table table);
}
