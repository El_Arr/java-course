package ru.kpfu.itis.god_and_preacher.model;

import java.io.Serializable;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class Hand implements Serializable {
    private List<Card> cards;

    public Hand() {
        this.cards = new ArrayList<>();
    }

    public void addCard(Card card) {
        cards.add(card);
    }

    public Card playCard(int index) {
        cards.remove(index);
        return cards.get(index);
    }

    public Card getCard(int index) {
        return cards.get(index);
    }

    public Card getCard(String title) {
        Pattern pattern = Pattern.compile(
                title.toLowerCase()
        );
        Matcher matcher;
        for (Card c : cards) {
            matcher = pattern.matcher(c.toString().toLowerCase());
            if (matcher.find()) {
                return c;
            }
        }
        return null;
    }

    @Override
    public String toString() {
        StringBuilder s = new StringBuilder();
        for (Card card : cards) {
            s.append(", ").append(card.toString());
        }
        return s.toString();
    }

}
