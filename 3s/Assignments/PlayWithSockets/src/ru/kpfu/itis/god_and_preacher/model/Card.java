package ru.kpfu.itis.god_and_preacher.model;

import java.io.Serializable;

public class Card implements Serializable {
    private String suit;
    private String type;

    public Card(String suit, String type) {
        this.suit = suit;
        this.type = type;
    }

    @Override
    public String toString() {
        return type + " " + suit;
    }
}
