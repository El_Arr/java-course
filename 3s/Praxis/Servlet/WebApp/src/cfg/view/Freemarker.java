package cfg.view;

import freemarker.template.Configuration;
import freemarker.template.TemplateExceptionHandler;

import java.io.File;
import java.io.IOException;
import java.util.Locale;

public class Freemarker {
    private static Freemarker freemarker = new Freemarker();
    private static Configuration cfg;

    private Freemarker() {
        try {
            cfg = new Configuration(Configuration.VERSION_2_3_20);
            cfg.setDirectoryForTemplateLoading(new File("C:\\Users\\Admin\\Git\\IT\\Praxis\\WebApp\\web\\templates"));
            cfg.setDefaultEncoding("UTF-8");
            cfg.setLocale(Locale.US);
            cfg.setTemplateExceptionHandler(TemplateExceptionHandler.RETHROW_HANDLER);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }

    public static Configuration getCfg() {
        return cfg;
    }
}
