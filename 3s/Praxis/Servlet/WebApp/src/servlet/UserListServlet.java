package servlet;

import cfg.view.Freemarker;
import entity.User;
import freemarker.template.Template;
import freemarker.template.TemplateException;
import repository.UserRepository;

import javax.servlet.http.*;
import java.io.IOException;
import java.util.HashMap;
import java.util.Map;

public class UserListServlet extends HttpServlet {

    protected void doPost(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException {
        for (Object user : UserRepository.getAllUsers()) {
            int id = ((User) user).getId();
            if (request.getParameter("delete_id=" + id) != null) {
                UserRepository.deleteUser(id);
                response.sendRedirect("/users");
            } else if (request.getParameter("edit_id=" + id) != null) {
                response.sendRedirect("/user/" + id);
            }
        }

    }

    protected void doGet(javax.servlet.http.HttpServletRequest request, javax.servlet.http.HttpServletResponse response)
            throws javax.servlet.ServletException, IOException {
        Map<String, Object> root = new HashMap<>();
        root.put("users", UserRepository.getAllUsers());
        Template temp = Freemarker.getCfg().getTemplate("users.ftl");
        try {
            temp.process(root, response.getWriter());
        } catch (TemplateException e) {
            e.printStackTrace();
        }
    }
}
